{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}

module ForExcel.Types where

import GHC.Int
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import Database.Persist.Postgresql(Single, unSingle)
import Text.RawString.QQ (r)
import Data.Text (Text, pack)
import qualified Text.Blaze.Html5 as H hiding (main)
import qualified Text.XML.Generator as XML

type IType =
    ( Single String
    , Single String
    , Single String
    , Single Double
    , Single String
    , Single Bool
    , Single String
    , Single Double
    )

typesXml :: [ IType ] -> BSL.ByteString
typesXml types =
    BSL.pack $ BS.unpack $
        XML.xrender $
            XML.doc XML.defaultDocInfo $
                XML.xelem "itemTypes" $
                    XML.xelems $ map
                        (\(group, name, uom, recuperation, planningMethod,
                                    localPurchase, customsCode, customsDuty) ->
                            XML.xelem
                                "itemType"
                                (XML.xattr "group" (pack $ unSingle group)
                                XML.<>
                                XML.xattr "name" (pack $ unSingle name)
                                XML.<>
                                XML.xattr "uom" (pack $ unSingle uom)
                                XML.<>
                                XML.xattr "recuperation" (pack $ show (unSingle recuperation))
                                XML.<>
                                XML.xattr "planningMethod" (pack $ unSingle planningMethod)
                                XML.<>
                                XML.xattr "localPurchase" (pack $ show (unSingle localPurchase))
                                XML.<>
                                XML.xattr "customsCode" (pack $ unSingle customsCode)
                                XML.<>
                                XML.xattr "customsDuty" (pack $ show (unSingle customsDuty))))
                        types 


typesSql :: Text 
typesSql = [r|
    SELECT
        it.group,
        it.name,
        it.uom,
        it.recuperation,
        it.planning_method,
        it.local_purchase,
        it.customs_code,
        it.customs_duty
    FROM
        item_type it
    ORDER BY
        it.group,
        it.name
|]
