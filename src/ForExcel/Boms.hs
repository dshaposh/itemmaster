{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}

module ForExcel.Boms where

import GHC.Int
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import Database.Persist.Postgresql(Single, unSingle)
import Text.RawString.QQ (r)
import Data.Text (Text, pack)
import qualified Text.XML.Generator as XML

type Bom =
    ( Single Int64
    , Single String
    , Single Int64
    , Single String
    , Single Double
    , Single String
    )

bomsXml :: [ Bom ] -> BSL.ByteString
bomsXml boms =
    BSL.pack $ BS.unpack $
        XML.xrender $
            XML.doc XML.defaultDocInfo $
                XML.xelem "boms" $
                    XML.xelems $ map
                        (\(code1c, idescr, piid, pidescr, conv, isSubAssembly) ->
                            XML.xelem
                                "bom"
                                (XML.xattr "code1c" (pack $ show (unSingle code1c))
                                XML.<>
                                XML.xattr "ItemDescription" (pack $ unSingle idescr)
                                XML.<>
                                XML.xattr "partId" (pack $ show (unSingle piid))
                                XML.<>
                                XML.xattr "PartDescription" (pack $ unSingle pidescr)
                                XML.<>
                                XML.xattr "Conversion" (pack $ show (unSingle conv))
                                XML.<>
                                XML.xattr "isSA" (pack $ unSingle isSubAssembly)))
                        boms 

bomsSql :: Text 
bomsSql = [r|
    SELECT
        i.code1_c as icode1C,
        i.description as idescription,
        pi.id as pid,
        itemdescr(pi, it.descr_tmpl) as pdescription,
        bom.qty as qty, 
        CASE WHEN i.item_type = pi.item_type THEN 'No' ELSE 'Yes' END as isSubAsembly 
    FROM
        bom 
    INNER JOIN
        item i 
    ON
        bom.item=i.id 
    INNER JOIN
        plan_item pi 
    ON
        bom.pitem=pi.id 
    INNER JOIN
        item_type it 
    ON
        pi.item_type=it.id 
    ORDER BY
        i.description,
        pdescription 
|]

