{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}

module ForExcel.Pitems where

import GHC.Int
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import Database.Persist.Postgresql(Single, unSingle)
import Text.RawString.QQ (r)
import Data.Text (Text, pack)
import qualified Text.Blaze.Html5 as H hiding (main)
import qualified Text.XML.Generator as XML


type Pitem =
    ( Single Int64
    , Single String
    , Single String
    , Single String
    , Single String
    , Single Double
    )
type ItemShort =
    ( Single Int64
    , Single String
    , Single String
    , Single Double
    )

pitemsXml :: [ (Pitem, ItemShort) ] -> BSL.ByteString
pitemsXml pitems =
    BSL.pack $ BS.unpack $
        XML.xrender $
            XML.doc XML.defaultDocInfo $
                XML.xelem "pitems" $
                    XML.xelems $ map
                        (\((id, tgroup, tname, descr, uom, recup),(icode,idescr,iuom,iprice)) ->
                            XML.xelem "row" $
                            XML.xelem "pitem"
                                (XML.xattr "id" (pack $ show (unSingle id))
                                XML.<>
                                XML.xattr "group" (pack $ unSingle tgroup)
                                XML.<>
                                XML.xattr "name" (pack $ unSingle tname)
                                XML.<>
                                XML.xattr "uom" (pack $ unSingle uom)
                                XML.<>
                                XML.xattr "recuperation" (pack $ show (unSingle recup))
                                XML.<#>
                                XML.xtext (pack (unSingle descr)))
                            XML.<>
                            XML.xelem "item"
                                (XML.xattr "code1c" (pack $ show (unSingle icode))
                                XML.<>
                                XML.xattr "uom" (pack $ unSingle iuom)
                                XML.<>
                                XML.xattr "price" (pack $ show (unSingle iprice))
                                XML.<#>
                                XML.xtext (pack (unSingle idescr))))
                        pitems 


pitemsPage :: [ (Pitem, ItemShort) ] -> H.Html
pitemsPage rows =
    H.docTypeHtml $ do
        H.head $ do
            H.title "Items"
        H.body $ do
            H.table $ do
                H.thead $ do
                    H.tr $ do
                        H.th "id"
                        H.th "Tgroup"
                        H.th "Tname"
                        H.th "Description"
                        H.th "UOM"
                        H.th "Recuperation"
                        H.th "Item Code1C"
                        H.th "Item Description"
                        H.th "Item UOM"
                        H.th "Item Price"
                H.tbody $ mapM_ pitemLine rows
        where
            pitemLine ((id, tgroup, tname, descr, uom, recup),(icode,idescr,iuom,iprice)) =
                H.tr $ do 
                    H.td $ H.toHtml $ unSingle id 
                    H.td $ H.toHtml $ unSingle tgroup
                    H.td $ H.toHtml $ unSingle tname
                    H.td $ H.toHtml $ unSingle descr
                    H.td $ H.toHtml $ unSingle uom 
                    H.td $ H.toHtml $ unSingle recup
                    H.td $ H.toHtml $ unSingle icode
                    H.td $ H.toHtml $ unSingle idescr
                    H.td $ H.toHtml $ unSingle iuom
                    H.td $ H.toHtml $ unSingle iprice


pitemsSql :: Text 
pitemsSql = [r|
    SELECT
        pi.id,
        it.group,
        it.name,
        itemdescr(pi, it.descr_tmpl) AS pdescription,
        it.uom,
        it.recuperation,
        COALESCE (bom2.code1_c, 0::integer) AS code1c,
        COALESCE (bom2.description, '') AS description,
        COALESCE (bom2.uom, '') as iuom,
        COALESCE (bom2.price, 0::float) as price
    FROM
        plan_item pi 
    INNER JOIN
        item_type it 
    ON
        pi.item_type=it.id 
    LEFT OUTER JOIN ( 
            SELECT
                bom.pitem,
                item.id,
                item.code1_c,
                item.description,
                item.uom,
                item.price 
            FROM
                bom 
            INNER JOIN
                item 
            ON
                bom.item=item.id 
            WHERE
                bom.is_main=true) bom2 
    ON
        bom2.pitem=pi.id 
    ORDER BY
        it.group,
        it.name,
        pdescription 
|]
