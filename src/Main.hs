{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE EmptyDataDecls #-}

{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE FlexibleInstances #-}

import qualified System.Directory
import qualified Data.ByteString as DBS

import Database.Persist
import Database.Persist.Postgresql
import Database.Persist.TH
import Database.Persist.Sql (SqlBackend, insert)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Trans.Reader (ReaderT) 
import Data.Text (Text, replace, concat, pack)
import Data.Pool (Pool)
import Control.Monad.Logger
import Data.Monoid ((<>))
import GHC.Int
import Text.RawString.QQ (r)

import Data.Aeson

import qualified Data.Text.Lazy as T
import qualified Web.Scotty as S
import qualified Text.Blaze.Html5 as H hiding (main)
import qualified Text.Blaze.Html5.Attributes as A
import qualified Text.Blaze.Html.Renderer.Pretty as R
import Network.Wai.Middleware.RequestLogger
import Network.Wai.Middleware.Static

--import qualified ForExcel
import qualified ForExcel.Items as FEI
import qualified ForExcel.Pitems as FEP
import qualified ForExcel.Boms as FEB
import qualified ForExcel.Types as FET

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
ItemType json
    name        Text
    group       Text
    uom         Text
    descrTmpl   Text
    recuperation Double default=0
    planningMethod Text default=''
    localPurchase Bool default=False
    customsCode Text default=''
    customsDuty Double default=0
    attr1Descr  Text default=''
    attr1Values Text default='' 
    attr2Descr  Text default=''
    attr2Values Text default=''
    attr3Descr  Text default=''
    attr3Values Text default=''
    attr4Descr  Text default=''
    attr4Values Text default=''
    attr5Descr  Text default=''
    attr5Values Text default=''
    note        Text default=''
    Name name
    deriving    Show

PlanItem json
    itemType    ItemTypeId
    price       Double default=0
    attr1       Text default=''
    attr2       Text default=''
    attr3       Text default=''
    attr4       Text default=''
    attr5       Text default=''
    TypeAttrs itemType attr1 attr2 attr3 attr4 attr5
    deriving    Show

Item json
    code1C      Int
    pn          Text default=''
    description Text
    uom         Text
    itemType    ItemTypeId
    price       Double
    packageSize Double default=0
    Code1C code1C
    deriving    Show

Bom json
    item        ItemId
    pitem       PlanItemId
    qty         Double
    isMain      Bool default=False
    ItemPitem item pitem

|]

doMigrations :: ReaderT SqlBackend IO ()
doMigrations = runMigration migrateAll

doDbStaff :: ReaderT SqlBackend IO ()
doDbStaff = do
    result :: [Single Text] <- rawSql itemDescrFunctionSql []
    liftIO $ print result

instance (ToJSON a) => ToJSON (Single a) where
    toJSON v = toJSON $ unSingle v

configFile :: String
configFile = "itemmaster.conf"

configDir :: String
configDir = "/opt/itemmaster/"

getConfFile = do
    isConfExist <- System.Directory.doesFileExist (configDir ++ configFile)
    return $ if isConfExist then configDir ++ configFile
                else configFile

type ItemExtAttr =
    ( Single String     -- type group
    , Single String     -- type name
    , Single Int16      -- boms count
    , Single Int16      -- boms qty
    , Single String     -- plan item description
    )
type PlanItemValue =
    ( Single Int64      -- id
    , Single Int64      -- type_id
    , Single String     -- uom
    , Single String     -- description
    , Single String     -- type name
    , Single String     -- type group
    )
prepareFilter1 :: [ (T.Text, T.Text) ] -> String
prepareFilter1 pairs =
    foldr (\pair result -> (T.unpack $ conditionStr pair) ++ result) "" pairs
    where
        conditionStr (_, "") = ""
        conditionStr ("code1c", value) = T.replace "#" value " AND item.code1_c::Text LIKE '%#%'"
        conditionStr ("pn", value) = T.replace "#" value " AND item.pn LIKE '%#%'"
        conditionStr ("description", value) = T.replace "#" (T.replace "*" "%" value) " AND item.description ILIKE '%#%'"
        conditionStr ("itype", value) = T.replace "#" value " AND item.item_type=#"
        conditionStr (_,_) = ""
    
prepareFilter2 :: [ (T.Text, T.Text) ] -> String
prepareFilter2 pairs =
    foldr (\pair result -> (T.unpack $ conditionStr pair) ++ result) "" pairs
    where
        conditionStr (_, "") = ""
        conditionStr ("pitype", value) = T.replace "#" value " AND pi.item_type=#"
        conditionStr (_,_) = ""
    

main :: IO ()
main = do
    confFile <- getConfFile
    connStr <- DBS.readFile confFile

    putStrLn $ show connStr

    pool <- runStdoutLoggingT $ createPostgresqlPool connStr 10
    runDb pool doMigrations
    S.scotty 3000 $ do
        S.middleware $ staticPolicy (noDots >-> addBase "client")
        S.middleware logStdoutDev

        S.get "/" $ do
            S.html $ T.pack $ R.renderHtml homePage

-----------------------------------------------
        S.get "/api/items" $ do
            params :: [S.Param] <- S.params
            let sql1 = Data.Text.replace "FILTER1" (pack $ prepareFilter1 params) itemsSql    
            let sql2 = Data.Text.replace "FILTER2" (pack $ prepareFilter2 params) sql1    
            res :: [ (Entity Item, ItemExtAttr) ] <- runDb pool (rawSql sql2 [])
            S.json res

        S.post "/api/items" $ do
            item <- S.jsonData :: S.ActionM Item
            newKey :: Key Item <- runDb pool (insert item)
            S.json newKey

        S.put "/api/items/:id" $ do
            itemId :: Int64 <- S.param "id" 
            let id :: Key Item = toSqlKey itemId
            item <- S.jsonData :: S.ActionM Item
            runDb pool (Database.Persist.Postgresql.replace id item)
            S.json itemId 

        S.delete "/api/items/:id" $ do
            itemId :: Int64 <- S.param "id" 
            let key :: Key Item = toSqlKey itemId
            runDb pool (delete key)
            S.json itemId
-----------------------------------------------
        S.get "/api/planitems-old" $ do
            res :: [Entity PlanItem] <- runDb pool (selectList [] [])
            S.json res

        S.get "/api/planitems" $ do
            res :: [ PlanItemValue ] <- runDb pool (rawSql planItemsSql [toPersistValue ("all" :: Text), toPersistValue (0 :: Int64)])
            S.json res

        S.get "/api/planitems/:id" $ do
            itemId :: Int64 <- S.param "id"
            let itemKey :: Key PlanItem = toSqlKey itemId
            res :: Maybe PlanItem <- runDb pool (get itemKey)
            S.json res

        S.put "/api/planitems/:id" $ do
            pitemId :: Int64 <- S.param "id" 
            let key :: Key PlanItem = toSqlKey pitemId
            pitem <- S.jsonData :: S.ActionM PlanItem
            runDb pool (Database.Persist.Postgresql.replace key pitem)
            res :: [ PlanItemValue ] <- runDb pool (rawSql planItemsSql [toPersistValue ("one" :: Text), toPersistValue key])
            S.json (head res)

        S.post "/api/planitems" $ do
            planItem <- S.jsonData :: S.ActionM PlanItem
            newKey :: Key PlanItem <- runDb pool (insert planItem)
            S.json newKey
--            res :: Maybe PlanItem <- runDb pool (get newKey)
--            res :: [ PlanItemValue ] <- runDb pool (rawSql planItemsSql [toPersistValue ("one" :: Text), toPersistValue newKey])
--            S.json (head res)

        S.delete "/api/planitems/:id" $ do
            itemId :: Int64 <- S.param "id" 
            let key :: Key PlanItem = toSqlKey itemId
            runDb pool (delete key)
            S.json itemId
-----------------------------------------------
        S.get "/api/types2" $ do
            res :: [Entity ItemType] <- runDb pool (selectList [] [])
            S.json res
        S.post "/api/types" $ do
            itype <- S.jsonData :: S.ActionM ItemType
            newKey :: Key ItemType <- runDb pool (insert itype)
            S.json newKey
        S.put "/api/types/:id" $ do
            typeId :: Int64 <- S.param "id" 
            let id :: Key ItemType = toSqlKey typeId
            itype <- S.jsonData :: S.ActionM ItemType
            runDb pool (Database.Persist.Postgresql.replace id itype)
            S.json typeId 
        S.delete "/api/types/:id" $ do
            typeId :: Int64 <- S.param "id" 
            let key :: Key ItemType = toSqlKey typeId
            runDb pool (delete key)
            S.json typeId
        S.get "/api/types" $ do
            res :: [(Entity ItemType, Single Double, Single Double)] <- runDb pool (rawSql itemTypesSql [])
            S.json res
        S.get "/api/types/:id/pitems" $ do
            typeId :: Int64 <- S.param "id" 
            let typeKey :: Key ItemType = toSqlKey typeId
            res :: [(Entity PlanItem, Single Int16, Single Int16, Single String)] <- runDb pool (rawSql pitemsByTypeSql [toPersistValue typeKey])
            S.json res
-----------------------------------------------
        S.get "/api/boms/byitem/:id" $ do
            itemId :: Int64 <- S.param "id"
            let itemKey :: Key Item = toSqlKey itemId
            res :: [(Entity Bom, Entity Item, Entity PlanItem)] <- runDb pool (rawSql bomsByItemSql [toPersistValue itemKey])
            S.json res

        S.get "/api/boms/byitype/:itype/summary" $ do
            itypeId :: Int64 <- S.param "itype"
            let itypeKey :: Key ItemType = toSqlKey itypeId
            res :: [Single Int64] <- runDb pool (rawSql bomsSummaryByIgroupSql [toPersistValue itypeKey])
            S.json res

        S.get "/api/boms/byitype/:itype/:pitype" $ do
            itypeId :: Int64 <- S.param "itype"
            pitypeId :: Int64 <- S.param "pitype"
            let itypeKey :: Key ItemType = toSqlKey itypeId
            let pitypeKey :: Key ItemType = toSqlKey pitypeId
            let filter = [ toPersistValue pitypeKey, toPersistValue itypeKey ]
            res :: [(Single Int64, Single Int16, Single Int16)] <- runDb pool (rawSql bomsSummaryByIgroupSql filter )
            S.json res

        S.get "/api/boms/bypart/:id" $ do
            itemId :: Int64 <- S.param "id"
            let itemKey :: Key PlanItem = toSqlKey itemId
            res :: [(Entity Bom, Entity Item, Entity PlanItem)] <- runDb pool (rawSql bomsByPartSql [toPersistValue itemKey])
            S.json res

        S.delete "/api/boms/:id" $ do
            bomId :: Int64 <- S.param "id" 
            let key :: Key Bom = toSqlKey bomId
            runDb pool (delete key)
            S.json bomId

        S.post "/api/boms" $ do
            bom <- S.jsonData :: S.ActionM Bom
            newKey :: Key Bom <- runDb pool (insert bom)
            res :: [(Entity Bom, Entity Item, Entity PlanItem)] <- runDb pool (rawSql bomByIdSql [toPersistValue newKey])
            S.json $ head res 

        S.put "/api/boms/:id" $ do
            bomId :: Int64 <- S.param "id" 
            let key :: Key Bom = toSqlKey bomId
            bom <- S.jsonData :: S.ActionM Bom
            let pItem = bomPitem bom
            runDb pool (updateWhere [BomPitem ==. pItem] [BomIsMain =. False])
            runDb pool (update key [BomIsMain =. True])
            S.json key

        S.get "/excel/items" $ do
            res :: [ FEI.Item ] <- runDb pool (rawSql FEI.itemsSql [])
--            S.html $ T.pack $ R.renderHtml $ FEI.itemsPage res
            S.setHeader "Content-Type" "text/xml; charset=utf-8"
            S.raw $ FEI.itemsXml res

        S.get "/excel/pitems" $ do
            res :: [ (FEP.Pitem,FEP.ItemShort) ] <- runDb pool (rawSql FEP.pitemsSql [])
            --S.html $ T.pack $ R.renderHtml $ ForExcel.pitemsPage res
            S.setHeader "Content-Type" "text/xml; charset=utf-8"
            S.raw $ FEP.pitemsXml res

        S.get "/excel/boms" $ do
            res :: [ FEB.Bom ] <- runDb pool (rawSql FEB.bomsSql [])
            S.setHeader "Content-Type" "text/xml; charset=utf-8"
            S.raw $ FEB.bomsXml res

        S.get "/excel/types" $ do
            res :: [ FET.IType ] <- runDb pool (rawSql FET.typesSql [])
            S.setHeader "Content-Type" "text/xml; charset=utf-8"
            S.raw $ FET.typesXml res

        S.get "/excel/items-old" $ do
            res :: [Entity Item] <- runDb pool (selectList [] [])
            S.html $ T.pack $ R.renderHtml $ itemsPage res


runDb :: (IsPersistBackend backend, MonadIO m, BaseBackend backend ~ SqlBackend) =>
                Pool backend -> ReaderT backend IO a -> m a
runDb pool query =
    liftIO (runSqlPool query pool)

homePage :: H.Html
homePage = H.docTypeHtml $ do
    H.head $ do
        H.title "ItemMaster"
        H.link H.! A.rel "stylesheet" H.! A.href "static/screen.css"
    H.body $ do
        H.div H.! A.id "root-node" $ ""
        H.script H.! A.type_ "text/javascript" H.! A.src "static/main.js" $ "" 
        H.script H.! A.type_ "text/javascript" $ 
            "var node=document.getElementById('root-node');" <>
            "var app=Elm.Main.embed(node)"

itemsPage :: [ Entity Item] -> H.Html
itemsPage items =
    H.docTypeHtml $ do
        H.head $ do
            H.title "Items"
        H.body $ do
            H.table $ do
                H.tr $ do
                    H.th "Description"
                    H.th "PN"
                    H.th "Code1C"
                    H.th "UOM"
                H.tbody $ mapM_ itemLine items
        where
            itemLine item =
                H.tr $ do 
                    H.td $ (H.toHtml . itemDescription . entityVal) item 
                    H.td $ (H.toHtml . itemPn . entityVal) item 
                    H.td $ (H.toHtml . itemCode1C . entityVal) item 
                    H.td $ (H.toHtml . itemUom . entityVal) item 


itemsSql :: Text 
itemsSql = [r|
    SELECT ??, t.group, t.name, COALESCE(cq.bomc,0::int), COALESCE(cq.bomq,0::int), COALESCE(cq.fdescr,'')
    FROM item 
    INNER JOIN item_type t
    ON item.item_type = t.id
    LEFT OUTER JOIN (
        SELECT item.id as id, count(bom.id) as bomc, sum(bom.qty) as bomq, pd.pdescr as fdescr
        FROM item
        INNER JOIN bom
        ON item.id = bom.item
        INNER JOIN plan_item pi
        ON bom.pitem = pi.id
        LEFT OUTER JOIN (
            SELECT bom.item, itemdescr(pi, it.descr_tmpl) as pdescr
            FROM bom
            INNER JOIN plan_item pi
            ON bom.pitem = pi.id
            INNER JOIN item_type it
            ON it.id = pi.item_type
            INNER JOIN item
            ON item.id = bom.item
            WHERE item.item_type = pi.item_type) pd
        ON pd.item = item.id
        WHERE 1=1 FILTER2
        GROUP BY item.id, pd.pdescr) cq
    ON cq.id =item.id
    WHERE 1=1 FILTER1
    ORDER BY t.group, t.id
    LIMIT 300
|]

    

planItemsSql :: Text 
planItemsSql = [r|
    SELECT
        pi.id,
        pi.item_type,
        it.uom,
        COALESCE(itemdescr(pi, it.descr_tmpl),'Error') as descr,
        it.name,
        it.group
    FROM plan_item pi
    INNER JOIN item_type it
    ON pi.item_type = it.id
    WHERE ?='all' OR pi.id=?
    GROUP BY pi.id, it.uom, descr, it.name, it.group
    ORDER BY it.group, it.name, descr
|]

pitemsByTypeSql :: Text
pitemsByTypeSql = [r|
    SELECT ??, COALESCE(analogs.qty, 0), COALESCE(boms.qty, 0), COALESCE(mains.description, '')
    FROM plan_item
    LEFT OUTER JOIN (
        SELECT bom.pitem, count(bom.id) as qty
        FROM bom
        INNER JOIN item
        ON item.id = bom.item
        INNER JOIN plan_item plan
        ON plan.id = bom.pitem
        WHERE plan.item_type = item.item_type
        GROUP BY pitem
    ) analogs
    ON analogs.pitem = plan_item.id
    LEFT OUTER JOIN (
        SELECT bom.pitem, count(bom.id) as qty
        FROM bom
        GROUP BY pitem
    ) boms
    ON boms.pitem = plan_item.id
    LEFT OUTER JOIN (
        SELECT bom.pitem, item.description
        FROM bom
        INNER JOIN item
        ON item.id = bom.item
        WHERE bom.is_main = true
    ) mains
    ON mains.pitem = plan_item.id
    WHERE plan_item.item_type = ?
    ORDER BY plan_item.attr1, plan_item.attr2, plan_item.attr3, plan_item.attr4, plan_item.attr5
|] 

itemTypesSql :: Text
itemTypesSql = [r|
    SELECT ??, COALESCE(it.icount,0::int), COALESCE(it.picount,0::int)
    FROM item_type
    INNER JOIN (
        SELECT it.id as id, it.icount as icount, count(pi.id) as picount
        FROM ( 
            SELECT it.id as id, count(i.id) as icount 
            FROM item_type it
            LEFT OUTER JOIN item i
            ON it.id = i.item_type
            GROUP BY it.id) it 
        LEFT OUTER JOIN plan_item pi
        ON it.id = pi.item_type
        GROUP BY it.id, it.icount ) it
    ON item_type.id=it.id
    ORDER BY item_type.group, item_type.name
|]

bomsByPartSql :: Text 
bomsByPartSql = [r|
    SELECT ??, ??, ??
    FROM bom
    INNER JOIN item
    ON bom.item = item.id
    INNER JOIN plan_item
    ON plan_item.id = bom.pitem
    WHERE bom.pitem = ?
    ORDER BY item.item_type, item.description
|]

bomByIdSql :: Text 
bomByIdSql = [r|
    SELECT ??, ??, ??
    FROM bom
    INNER JOIN item
    ON bom.item = item.id
    INNER JOIN plan_item
    ON plan_item.id = bom.pitem
    WHERE bom.id = ?
|]

bomsByItemSql :: Text 
bomsByItemSql = [r|
    SELECT ??, ??, ??
    FROM bom
    INNER JOIN item
    ON bom.item = item.id
    INNER JOIN plan_item
    ON plan_item.id = bom.pitem
    WHERE bom.item = ?
|]
bomsSummaryByIgroupSql :: Text 
bomsSummaryByIgroupSql = [r|
    SELECT pi.item_type
    FROM bom
    INNER JOIN item
    ON bom.item = item.id
    INNER JOIN plan_item pi
    ON pi.id = bom.pitem
    WHERE item.item_type = ?
    GROUP BY pi.item_type
|]
bomsStatByIgroupSql :: Text 
bomsStatByIgroupSql = [r|
    SELECT item.id, bs.iqty, bs.tqty
    FROM item
    LEFT OUTER JOIN (
        SELECT bom.item as item, count(bom.id) as iqty, sum(bom.qty) as tqty
        FROM bom
        INNER JOIN plan_item pi
        ON bom.pitem = pi.id
        WHERE  pi.item_type = ?
        GROUP BY item) bs
    ON item.id = bs.item
    WHERE item.item_type = ?
|]

itemDescrFunctionSql :: Text
itemDescrFunctionSql = [r|
    CREATE OR REPLACE FUNCTION
        itemdescr (r plan_item, tmpl text)
    RETURNS text AS $$

        DECLARE
            description text :='';
        BEGIN

            IF tmpl is null THEN
                RETURN null;
            END IF;

            IF r.attr1 is not null THEN
                description =replace(tmpl, '|1|', r.attr1);
            END IF;
            IF r.attr2 is not null THEN
                description =replace(description, '|2|', r.attr2);
            END IF;
            IF r.attr3 is not null THEN
                description =replace(description, '|3|', r.attr3);
            END IF;
            IF r.attr4 is not null THEN
                description =replace(description, '|4|', r.attr4);
            END IF;
            IF r.attr5 is not null THEN
                description =replace(description, '|5|', r.attr5);
            END IF;

            RETURN description;
        END;
    $$ LANGUAGE plpgsql
|]
