var gulp=require('gulp'),
    rsync=require('gulp-rsync'),
    filelog=require('gulp-filelog');

var rsyncOptions={
        hostname: 'root@10.216.1.38',
        destination: '/opt/itemmaster/',
        emptyDirectories: true
    };

gulp.task('deploy:server', function(){
    var rootFrom ='.cabal-sandbox/bin/';
    rsyncOptions.root = rootFrom;
    return gulp.src(rootFrom + 'itemmaster')
    .pipe(filelog())
    .pipe(rsync(rsyncOptions));
});

gulp.task('deploy:static', function(){
    var rootFrom ='.';
    rsyncOptions.root = rootFrom;
    return gulp.src(rootFrom + '/client/static/**')
    .pipe(filelog())
    .pipe(rsync(rsyncOptions));
});

gulp.task('deploy:all', ['deploy:server', 'deploy:static']);

