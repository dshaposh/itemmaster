module ItemTypes exposing (..)

import Task
import Json.Decode as Decode exposing (field, index)
import Json.Decode.Pipeline exposing (decode, custom)
import Http
import Html exposing (..)
import Html.Events exposing (onClick)
import Html.Attributes exposing (href, rowspan, colspan, class)
import Helper
import GlobalContext exposing (ItemType, GlobalContext)
import Set exposing (..)
import ItemTypeEdit as ITE exposing (..)

import PlanItems2 as PlanItems exposing (..)
import Items as Items exposing (..)

itemTypeDecoder : Decode.Decoder ItemType
itemTypeDecoder =
    decode ItemType
        |> custom ( index 0 (field "id" Decode.int))
        |> custom ( index 0 (field "name" Decode.string))
        |> custom ( index 0 (field "group" Decode.string))
        |> custom ( index 0 (field "uom" Decode.string))
        |> custom ( index 0 (field "recuperation" Decode.float))
        |> custom ( index 0 (field "planningMethod" Decode.string))
        |> custom ( index 0 (field "localPurchase" Decode.bool))
        |> custom ( index 0 (field "customsCode" Decode.string))
        |> custom ( index 0 (field "customsDuty" Decode.float))
        |> custom ( index 0 (field "descrTmpl" Decode.string))
        |> custom ( index 0 (field "attr1Descr" Decode.string))
        |> custom ( index 0 (field "attr1Values" Decode.string))
        |> custom ( index 0 (field "attr2Descr" Decode.string))
        |> custom ( index 0 (field "attr2Values" Decode.string))
        |> custom ( index 0 (field "attr3Descr" Decode.string))
        |> custom ( index 0 (field "attr3Values" Decode.string))
        |> custom ( index 0 (field "attr4Descr" Decode.string))
        |> custom ( index 0 (field "attr4Values" Decode.string))
        |> custom ( index 0 (field "attr5Descr" Decode.string))
        |> custom ( index 0 (field "attr5Values" Decode.string))
        |> custom ( index 0 (field "note" Decode.string))
        |> custom ( index 1 Decode.int)
        |> custom ( index 2 Decode.int)

listItemTypesDecoder : Decode.Decoder (List ItemType)
listItemTypesDecoder = Decode.list itemTypeDecoder
           
type Msg
    = FpTypesFetched (List ItemType)
    | FpTypeSaved ItemType
    | FpTypeDeleted Int
    | FpShowItems ItemType
    | FpError String
    | FetchDone (Result Http.Error (List ItemType)) 
    | DeleteDone (Result Http.Error Int) 
    | EditCompMsg ITE.Msg
    | DoEdit (Maybe ItemType)
    | DoDelete ItemType
    | DoEditClose
    | GroupSelected String
    | ItemsMsg Items.Msg
    | PlanItemsMsg PlanItems.Msg
    | DoShowItems ItemType
    | DoShowPlanItems ItemType
    | DoClosePlanItems
    | DoCloseItems

itemsCompMsgConv : Items.Msg -> Msg
itemsCompMsgConv msg =
    case msg of
        Items.FpDone _ -> DoCloseItems
        Items.FpError errMsg -> FpError errMsg
        Items.FpShowPlanItems itemType -> DoShowPlanItems itemType
        _ -> ItemsMsg msg

planItemsCompMsgConv : PlanItems.Msg -> Msg
planItemsCompMsgConv msg  =
    case msg of
        PlanItems.FpError errMsg -> FpError errMsg
        PlanItems.FpBack -> DoClosePlanItems
        PlanItems.FpShowItems itemType -> DoShowItems itemType
        _ -> PlanItemsMsg msg

editCompMsgConv : ITE.Msg -> Msg
editCompMsgConv msg =
    case msg of
        ITE.FpBack ->
            DoEditClose
        ITE.FpTypeSaved savedType ->
            FpTypeSaved savedType
        ITE.FpError errMsg ->
            FpError errMsg
        _ as childMsg ->
            EditCompMsg  childMsg

type ActiveComp
    = Me
    | EditComp ITE.Model
    | ItemsComp Items.Model
    | PlanItemsComp PlanItems.Model

type alias Model =
    { activeComp : ActiveComp
    , selectedGroup : String
    }

initialModel : Model
initialModel =
    { activeComp = Me
    , selectedGroup = ""
    }

url : String
url = "/api/types"

fetch : Cmd Msg
fetch = Http.send FetchDone (Http.get url listItemTypesDecoder)

delete : ItemType -> Cmd Msg
delete item = Http.send DeleteDone (Helper.httpDelete url item.id)


update : GlobalContext -> Msg -> Model -> (Model, Cmd Msg)
update gc msg model =
    case msg of
        FpTypesFetched data ->
            Debug.crash "This message should have been intercepted by the parent."
        FpTypeSaved savedType ->
            Debug.crash "This message should have been intercepted by the parent."
        FpTypeDeleted id ->
            Debug.crash "This message should have been intercepted by the parent."
        FpShowItems itype ->
            Debug.crash "This message should have been intercepted by the parent."
        FpError _ ->
            Debug.crash "This message should have been intercepted by the parent."

        FetchDone (Ok newTypes) ->
            let
                newGroups = Set.toList <| List.foldl (\itype groups -> Set.insert itype.group groups) Set.empty newTypes
            in
                (model, Task.perform FpTypesFetched (Task.succeed newTypes))
        FetchDone (Err errMsg) ->
            (model, Task.succeed (toString errMsg) |> Task.perform FpError)

        DeleteDone (Ok id) ->
            (model,Task.perform FpTypeDeleted (Task.succeed id))
        DeleteDone (Err errMsg) ->
            (model, Task.succeed (toString errMsg) |> Task.perform FpError)

        EditCompMsg subMsg ->
            case model.activeComp of
                EditComp subModel ->
                    let (comp, cmd) = ITE.update gc subMsg subModel
                    in ({model| activeComp = EditComp comp}, Cmd.map editCompMsgConv cmd)
                _ -> (model, Cmd.none)

        DoEdit itype ->
            ({model|activeComp = EditComp ITE.initialModel}
             ,Task.perform EditCompMsg <| Task.succeed (ITE.Activate itype))
        DoDelete itype ->
            (model,delete itype)
        DoEditClose ->
            ({model|activeComp = Me},Cmd.none)
        GroupSelected igroup ->
            let
                newGroup =
                    if igroup == model.selectedGroup then
                        ""
                    else
                        igroup
            in
                ({model|selectedGroup = newGroup},Cmd.none)

        ItemsMsg subMsg ->
            case model.activeComp of
                ItemsComp subModel ->
                    let
                        (newModel, cmd) = Items.update gc subMsg subModel
                    in
                        ({model|activeComp = ItemsComp newModel}, Cmd.map itemsCompMsgConv cmd)
                _ -> (model, Cmd.none)

        PlanItemsMsg subMsg ->
            case model.activeComp of
                PlanItemsComp subModel ->
                    let
                        (mbNewModel, cmd) = PlanItems.update gc subMsg subModel
                    in
                        case mbNewModel of
                            Nothing -> (model, Cmd.none)
                            Just newModel ->
                                ({model|activeComp = PlanItemsComp newModel}, Cmd.map planItemsCompMsgConv cmd)
                _ -> (model, Cmd.none)

        DoShowItems itype ->
            let eFilter = Items.emptyFilter
                filter = {eFilter | itype = toString itype.id}
            in
                (,)
                    {model|activeComp = ItemsComp <| Items.setFilter filter}
                    (Cmd.map itemsCompMsgConv <| Items.fetch filter)
        DoShowPlanItems itype ->
            (,)
                {model|activeComp = PlanItemsComp <| PlanItems.initialModel itype}
                (Cmd.map planItemsCompMsgConv <| PlanItems.fetch itype.id)
        DoClosePlanItems ->
            ({model|activeComp = Me}, Cmd.none)
        DoCloseItems ->
            ({model|activeComp = Me}, Cmd.none)


summaryView : GlobalContext -> Model -> Html Msg
summaryView gc model =
    case model.activeComp of
        Me -> 
            div []
                [ h1 [] [text "Summary of item types"]
                , div []
                    [ table []
                        [ thead []
                            [ tr []
                                [ th [] []
                                , th [] [ text "Group" ]
                                , th [] [ text "Name" ]
                                , th [] [ text "UOM" ]
                                , th [] [ text "Items Qty" ]
                                , th [] [ text "Planning Items Qty" ]
                                , th [] [ text "Action" ]
                                ]
                            ]
                        , tbody [] <| List.concat <| List.map (\igroup -> summaryGroupView gc model igroup) gc.itemGroups
                        ]
                    ]
                ]
        ItemsComp model ->
            Html.map itemsCompMsgConv (Items.compView gc model)
        PlanItemsComp model ->
            Html.map planItemsCompMsgConv (PlanItems.itemsView gc model)
        EditComp model ->
            Html.map editCompMsgConv (ITE.compView gc model)

summaryGroupView : GlobalContext -> Model -> String -> List (Html Msg)
summaryGroupView gc model igroup  =
    let
        mark = if (model.selectedGroup == igroup) then "-" else "+"
        (itemsQty, pitemsQty) =
            List.foldr
                (\itype sum ->
                    if itype.group == igroup then
                        ( (Tuple.first sum) + itype.itemsQty
                        , (Tuple.second sum) + itype.pItemsQty
                        )
                    else
                        sum)
                (0,0)
                gc.itemTypes
    in
        [ tr [ class "summaryRow action", onClick <| GroupSelected igroup ]
            [ td [] [ text mark ]
            , td [ class "description" ] [ text igroup ]
            , td [] []
            , td [] []
            , td [] [ text <| toString itemsQty ]
            , td [] [ text <| toString pitemsQty ]
            , td [] []
            ]
        ]
        ++
        if igroup == model.selectedGroup then
            List.map (\itype -> summaryLineView itype) <| List.filter (\itype -> itype.group == model.selectedGroup) gc.itemTypes
        else []


summaryLineView : ItemType -> Html Msg
summaryLineView itemType =
    tr []
        [ td [] []
        , td [] [ text itemType.group ]
        , td [] [ text itemType.name ]
        , td [] [ text itemType.uom ]
        , td [] [ a [ DoShowItems itemType |> onClick, href "#"] [ text <| toString itemType.itemsQty ] ]
        , td [] [ a [ DoShowPlanItems itemType |> onClick, href "#"] [ text <| toString itemType.pItemsQty ] ]
        , td [] 
            [ button [onClick <| DoDelete itemType] [ text "x"]
            , button [onClick <| DoEdit <| Just itemType] [ text "e"]
            ]
        ]
