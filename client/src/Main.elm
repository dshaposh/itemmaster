module Main exposing (..)

import Task exposing (..)
import Html exposing (..)
import Html.Events exposing (onClick)
import Html.Attributes exposing (class, classList)
import GlobalContext as GC exposing (..)
import Items exposing (..)
import Models exposing (Item)
import ItemTypes exposing (..)
import EditItem exposing (..)
import ViewItem exposing (..)

import Models as M exposing (..)

type Msg
    = ErrMsg String
    | DoClearError Int
    | ItemsMsg Items.Msg
    | ItemTypesMsg ItemTypes.Msg
    | LoadTypes
    | DoShowItems Items.Filter
    | DoShowSummary
    | ItemTypesUpdated (List ItemType)
    | ItemTypeSaved ItemType
    | ItemTypeDeleted Int


itemsMsgConverter : Items.Msg -> Msg
itemsMsgConverter msg =
    case msg of
        Items.FpError errMsg ->
            ErrMsg errMsg
        _ as forChildMsg ->
            ItemsMsg forChildMsg

itemTypesMsgConverter : ItemTypes.Msg -> Msg
itemTypesMsgConverter msg =
    case msg of
        ItemTypes.FpTypesFetched typesList ->
            ItemTypesUpdated typesList
        ItemTypes.FpTypeSaved itype ->
            ItemTypeSaved itype
        ItemTypes.FpTypeDeleted id ->
            ItemTypeDeleted id
        ItemTypes.FpShowItems itype ->
            let filter = Items.emptyFilter
            in DoShowItems {filter|itype = toString itype.id}
        ItemTypes.FpError errMsg ->
            ErrMsg errMsg
        _ as childMsg ->
            ItemTypesMsg childMsg

type Component
    = SummaryComp
    | ItemsComp

type alias Model =
    { pItems : List M.PlanItem
    , globalContext : GC.GlobalContext
    , activeComp : Component
    , itemsComp : Items.Model
    , itemTypesComp : ItemTypes.Model
    , errors: List (Int, String)
    }

initialModel : Model
initialModel =
    { pItems = []
    , globalContext = GC.initialData
    , activeComp = SummaryComp
    , itemsComp = Items.initialModel 
    , itemTypesComp = ItemTypes.initialModel 
    , errors = []
    }

update : Msg -> Model -> (Model, Cmd Msg)
update msg model = 
    case msg of
        ErrMsg subMsg ->
            let 
                newId = (List.length model.errors) + 1
                newErrorList = (newId, subMsg) :: model.errors
            in
                ({model|errors = newErrorList}, Cmd.none)

        DoClearError id ->
            let
                newErrorList = List.filter (\(msgId,_) -> msgId /= id) model.errors
            in
                ({model|errors = newErrorList}, Cmd.none)

        ItemsMsg subMsg ->
            let
                (newItemsComp, cmd) = Items.update model.globalContext subMsg model.itemsComp 
            in
                ({model| itemsComp = newItemsComp}, Cmd.map itemsMsgConverter cmd)
        ItemTypesMsg subMsg ->
            let
                (newComp, cmd) = ItemTypes.update model.globalContext subMsg model.itemTypesComp
            in
                ({model| itemTypesComp = newComp}, Cmd.map itemTypesMsgConverter cmd)

        LoadTypes ->
            (model, Cmd.map ItemTypesMsg ItemTypes.fetch)


        DoShowSummary ->
            ({model| activeComp = SummaryComp}, Cmd.none) 

        DoShowItems filter ->
            ({model| activeComp = ItemsComp, itemsComp = Items.setFilter filter}, Cmd.map itemsMsgConverter (Items.fetch filter))

        ItemTypesUpdated iTypes ->
            let gc = model.globalContext
                newGc = {gc|itemTypes = iTypes}
            in ({model|globalContext = newGc}, Cmd.none)
        ItemTypeSaved iType ->
            let gc = model.globalContext
                newTypes = iType :: (List.filter (\itype -> itype.id /= iType.id) gc.itemTypes)
                newgc = {gc|itemTypes = newTypes}
            in ({model|globalContext = newgc}, Cmd.none)
        ItemTypeDeleted id ->
            let gc = model.globalContext
                newTypes = List.filter (\itype -> itype.id /= id) gc.itemTypes
                newgc = {gc|itemTypes = newTypes}
            in ({model|globalContext = newgc}, Cmd.none)


init : (Model, Cmd Msg)
init = (initialModel, Cmd.batch [Cmd.map ItemTypesMsg ItemTypes.fetch])

subscriptions : Model -> Sub Msg
subscriptions model = Sub.none

view : Model -> Html Msg
view model =
    div []
        [ div [ class "contentWoFooter" ](
            [div [ class "header" ]
                [ ul [class "navbar"] 
                    [ li
                        [ onClick DoShowSummary
                        , classList [("selected",model.activeComp == SummaryComp)]
                        ]
                        [ text "Summary"]
                    , li
                        [ onClick <| DoShowItems <| Items.emptyFilter
                        , classList [("selected",model.activeComp == ItemsComp)]
                        ]
                        [ text "Item master"]
                    ]
                ]
            ] 
            ++ (viewErrors model.errors) ++
            [ div [ class "content" ] 
                ( case model.activeComp of
                    SummaryComp ->
                        [ Html.map itemTypesMsgConverter (ItemTypes.summaryView model.globalContext model.itemTypesComp)
                        ]
                    ItemsComp ->
                        [ Html.map itemsMsgConverter (Items.compView model.globalContext model.itemsComp)
                        ]
                )
            ])
        , div [ class "footer" ]
            [ div [ class "footer-data" ]
                [ text "Item master manager" 
                ]
            ]
        ]

viewErrors : List (Int, String) -> List (Html Msg)
viewErrors errors =
    List.map
        (\(id,msg) -> div [class "errorMessage", onClick <| DoClearError id ] [text msg ])
        errors

main =
    Html.program { init = init, view = view, update = update, subscriptions = subscriptions }

