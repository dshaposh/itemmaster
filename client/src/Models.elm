module Models exposing (..)

import Json.Decode as Decode exposing (index, field)

type alias Item =
    { id :          Int
    , code1C :      Int
    , pn :          String
    , description : String
    , uom :         String
    , itemType :    Int
    , price :       Float
    , packageSize:  Float
    }

itemDecoder : Decode.Decoder Item
itemDecoder =
    Decode.map8
        Item
        (field "id" Decode.int)
        (field "code1C" Decode.int)
        (field "pn" Decode.string)
        (field "description" Decode.string)
        (field "uom" Decode.string)
        (field "itemType" Decode.int)
        (field "price" Decode.float)
        (field "packageSize" Decode.float)

--#############################################################################
type alias PlanItem =
    { id : Int
    , piTypeId : Int
    , piUom : String
    , piPrice : Float
    , piDescr : String
    , piType : String
    , piGroup : String
    }

planItemDecoder : Decode.Decoder PlanItem
planItemDecoder =
    Decode.map7
        PlanItem
        (index 0 Decode.int)
        (index 1 Decode.int)
        (index 2 Decode.string)
        (Decode.succeed 0)
        (index 3 Decode.string)
        (index 4 Decode.string)
        (index 5 Decode.string)

listPitemsDecoder : Decode.Decoder (List PlanItem)
listPitemsDecoder = Decode.list planItemDecoder

