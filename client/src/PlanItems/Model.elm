module PlanItems.Model exposing (..)

import Task
import Http
import Html exposing (..)
import Html.Events exposing (onClick, onInput)
import Html.Attributes exposing (type_, value, selected, disabled, class)
import Json.Decode as Decode exposing (index, field, string, int, float)
import Json.Encode as Encode
import Regex as R

import Models as M exposing (..)
import GlobalContext as GC exposing (..)
import Helper as H exposing (..)

fetchUrl: Int -> String
fetchUrl id = "/api/types/" ++ (toString id) ++ "/pitems"

url : String
url = "/api/planitems"

type alias Attributes =
    { itypeId   : Int
    , price     : Float
    , attr1     : String
    , attr2     : String
    , attr3     : String
    , attr4     : String
    , attr5     : String
    , id        : Int
    }

type alias Model =
    { id : Int
    , attributes : Attributes
    , analogQty : Int
    , bomsQty : Int
    , mainItemDescr : String
    }

description : List GC.ItemType -> Attributes -> String
description itypes model =
    let mbItype = List.head <| List.filter (\itype -> itype.id == model.itypeId ) itypes
    in case mbItype of
        Nothing -> "Item type not found"
        Just itype ->
            R.replace (R.AtMost 1) (R.regex "\\|1\\|") (\_ -> model.attr1) itype.descrTmpl |>
            R.replace (R.AtMost 1) (R.regex "\\|2\\|") (\_ -> model.attr2) |> 
            R.replace (R.AtMost 1) (R.regex "\\|3\\|") (\_ -> model.attr3) |>
            R.replace (R.AtMost 1) (R.regex "\\|4\\|") (\_ -> model.attr4) |>
            R.replace (R.AtMost 1) (R.regex "\\|5\\|") (\_ -> model.attr5)

decoder : Decode.Decoder Model 
decoder =
    Decode.map5
        Model
        (index 0 (field "id" int))
        (index 0 attributesDecoder)
        (index 1 int)
        (index 2 int)
        (index 3 string)

listDecoder : Decode.Decoder (List Model)
listDecoder = Decode.list decoder


fetch : Int -> (Result Http.Error (List Model) -> b) -> Cmd b 
fetch itemTypeId msg =
    Http.send msg (Http.get (fetchUrl itemTypeId) listDecoder)

emptyAttributes : Int -> Attributes
emptyAttributes itypeId =
    { itypeId = itypeId
    , price = 0
    , attr1 = ""
    , attr2 = ""
    , attr3 = ""
    , attr4 = ""
    , attr5 = ""
    , id = 0
    }
emptyModel : Int ->  Model
emptyModel itypeId =
    { id = 0
    , attributes = emptyAttributes itypeId
    , analogQty = 0
    , bomsQty = 0
    , mainItemDescr = "" 
    }

validateModel : Attributes -> GC.ItemType -> Bool
validateModel attributes itype =
        (attributes.itypeId == itype.id) &&
        (attributes.price > 0) &&
        (itype.attr1 == "") || (not <| String.isEmpty attributes.attr1) &&
        (itype.attr2 == "") || (not <| String.isEmpty attributes.attr2) &&
        (itype.attr3 == "") || (not <| String.isEmpty attributes.attr3) &&
        (itype.attr4 == "") || (not <| String.isEmpty attributes.attr4) &&
        (itype.attr5 == "") || (not <| String.isEmpty attributes.attr5)
    

encode : Attributes -> Encode.Value
encode attributes =
    Encode.object
        [ ("itemType", Encode.int attributes.itypeId)
        , ("price", Encode.float attributes.price)
        , ("attr1", Encode.string attributes.attr1)
        , ("attr2", Encode.string attributes.attr2)
        , ("attr3", Encode.string attributes.attr3)
        , ("attr4", Encode.string attributes.attr4)
        , ("attr5", Encode.string attributes.attr5)
        ]

attributesDecoder : Decode.Decoder Attributes
attributesDecoder =
    Decode.map8
        Attributes
        (field "itemType" int)
        (field "price" float)
        (field "attr1" string)
        (field "attr2" string)
        (field "attr3" string)
        (field "attr4" string)
        (field "attr5" string)
        (field "id" int)

save : Maybe Int -> Attributes -> (Result Http.Error Int -> b) -> Cmd b 
save mbId model msg =
    case mbId of
        Nothing ->
            Http.send msg (Http.post url (Http.jsonBody <| encode model) int) 
        Just id ->
            Http.send msg (H.httpPut url id (Http.jsonBody <| encode model) int) 

delete : Int -> (Result Http.Error Int -> b) -> Cmd b
delete itemId msg = Http.send msg (H.httpDelete url itemId)
