module ViewPlanItem2 exposing (..)

import Task
import Http
import Html exposing (..)
import Html.Events exposing (onClick, onInput)
import Html.Attributes exposing (type_, value, selected, disabled)

import Models exposing (Item)
import PlanItems.Model as PlanItem exposing (Model)
import GlobalContext as GC exposing (..)
import Boms as Boms exposing (..)
import PlanItemEdit as IE exposing (..)
import Items exposing (..)
import Helper as H exposing (..)

type Msg
    = FpDone (Maybe Models.PlanItem)
    | FpError String
    | DoEditItem
    | DoSearchItem
    | EditPlanItemCompMsg IE.Msg
    | BomsCompMsg Boms.Msg
    | ItemsCompMsg Items.Msg
    | ItemSelected (Maybe Item)

editBomCompMsgConv : Boms.Msg -> Msg
editBomCompMsgConv msg =
   case msg of
       Boms.FpDoSearchItem -> DoSearchItem
       Boms.FpError errMsg -> FpError errMsg
       _ -> BomsCompMsg msg

editPlanItemCompMsgConv : IE.Msg -> Msg
editPlanItemCompMsgConv msg =
    case msg of
        IE.FpDone _ -> FpDone Nothing 
        IE.FpError errMsg -> FpError errMsg
        _ -> EditPlanItemCompMsg msg

itemsCompMsgConv : Items.Msg -> Msg
itemsCompMsgConv msg =
    case msg of
        Items.FpDone subMsg -> ItemSelected subMsg
        _ -> ItemsCompMsg msg


type alias Model =
    { planItem : PlanItem.Attributes
    , editBomCompModel : Boms.Model
    , editPlanItemCompModel : IE.Model
    , itemsCompModel : Items.Model
    , isItemsCompActive : Bool
    }


initialModel : GC.GlobalContext -> PlanItem.Attributes -> Model
initialModel gc pitem =
    let
        mbItype = List.head <| List.filter (\itype -> itype.id == pitem.itypeId) gc.itemTypes
    in
        case mbItype of
            Nothing -> Debug.crash "ItemType shall be defined here."
            Just itype ->
                { planItem = pitem
                , editBomCompModel = Boms.setPart (Just pitem) Boms.initialModel
                , editPlanItemCompModel = IE.initialModel itype (Just pitem)
                , itemsCompModel = Items.initialModel
                , isItemsCompActive = False
                }

 
activate : List Models.PlanItem -> PlanItem.Model -> Cmd Msg
activate pItems item =
        Cmd.map editBomCompMsgConv <| Boms.fetchBomByPart item.id

update : GC.GlobalContext -> Msg -> Model -> (Model, Cmd Msg)
update gc msg model =
    case msg of
        FpDone errMsg ->
            Debug.crash "This message should have been intercepted by the parent"
        FpError errMsg ->
            Debug.crash "This message should have been intercepted by the parent"

        ItemSelected mItem ->
            let
                newBomsCompModel = Boms.setItem mItem model.editBomCompModel
            in
                (,)
                    {model
                    | isItemsCompActive = False
                    , editBomCompModel = newBomsCompModel
                    }
                    Cmd.none

        DoEditItem ->
            (model, Cmd.none)

        EditPlanItemCompMsg subMsg ->
            let (newModel, cmd) = IE.update gc subMsg model.editPlanItemCompModel
            in ({model|editPlanItemCompModel = newModel}, Cmd.map editPlanItemCompMsgConv cmd)

        BomsCompMsg subMsg ->
            let (newModel, cmd) = Boms.update gc subMsg model.editBomCompModel
            in ({model|editBomCompModel = newModel}, Cmd.map editBomCompMsgConv cmd)

        ItemsCompMsg subMsg ->
                let (newModel, cmd) = Items.update gc subMsg model.itemsCompModel
                in ({model|itemsCompModel = newModel}, Cmd.map itemsCompMsgConv cmd)
        DoSearchItem ->
            ({model|isItemsCompActive = True}, Cmd.none)


compView : GC.GlobalContext -> Model -> Html Msg
compView gc model =
    if model.isItemsCompActive then
        div []
            [ h1[] [ text "Plan Item description - TBD" ]
            , Html.map itemsCompMsgConv (Items.selectView  model.itemsCompModel)
            ]
    else
        div []
            [ Html.map editPlanItemCompMsgConv <| IE.itemEditView gc model.editPlanItemCompModel
--            , Html.map editPlanItemCompMsgConv <| IE.itemView gc model.editPlanItemCompModel
            , h3 [][text "This item is connected with"]
            , Html.map editBomCompMsgConv <| Boms.viewBomItems model.editBomCompModel
            , Html.map editBomCompMsgConv <| Boms.newItem model.editBomCompModel
            ]
