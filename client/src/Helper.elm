module Helper exposing (..)

import Http
import Json.Decode as Decode

httpDelete : String -> Int -> Http.Request Int 
httpDelete url id =
    Http.request
        { method = "DELETE"
        , headers = []
        , url = url ++ "/" ++ (toString id)
        , body = Http.emptyBody
        , expect = Http.expectJson Decode.int
        , timeout = Nothing
        , withCredentials = False
        }

httpPut : String -> Int -> Http.Body -> Decode.Decoder value -> Http.Request value 
httpPut url id body decoder =
    Http.request
        { method = "PUT"
        , headers = []
        , url = url ++ "/" ++ (toString id)
        , body = body
        , expect = Http.expectJson decoder
        , timeout = Nothing
        , withCredentials = False
        }

orMaybe : (a -> b) -> Maybe a -> Maybe b -> Maybe b
orMaybe f a1 default =
    case a1 of
        Nothing -> default 
        Just ja -> Maybe.map f a1 

toStringM : Float -> String
toStringM f = (toFloat <| round <| f*100) / 100|> toString
