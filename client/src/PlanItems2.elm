module PlanItems2 exposing (..)

import Task
import Http
import Html exposing (..)
import Html.Events exposing (onDoubleClick, onClick, onInput)
import Html.Attributes exposing (type_, value, selected, disabled, class)
import Json.Decode as Decode exposing (index, string, int, float)
import Json.Encode as Encode

import Models as M exposing (..)
import GlobalContext as GC exposing (..)
import Helper as H exposing (..)

--import EditPlanItem exposing (..)
import PlanItems.Model as PlanItem exposing (..)
import PlanItemEdit as ItemEdit exposing (..)
import ViewPlanItem2 as VI exposing (..)

type ActiveComp 
    = Me
    | ItemEditComp

type alias Model =
    { itemType : ItemType
    , items : List PlanItem.Model
    , itemEditCM : ItemEdit.Model
    , viewItemCM : Maybe VI.Model
    , activeComp : ActiveComp
    }

initialModel : ItemType ->  Model 
initialModel itemType =
    { itemType = itemType
    , items = []
    , itemEditCM = ItemEdit.initialModel itemType Nothing
    , viewItemCM = Nothing
    , activeComp = Me
    }

fetch : Int -> Cmd Msg
fetch itypeId =
    PlanItem.fetch itypeId FetchDone

type Msg
    = FpError String
    | FpBack
    | FpShowItems GC.ItemType
    | FetchDone (Result Http.Error (List PlanItem.Model)) 
    | DeleteDone (Result Http.Error Int) 
    | ItemEditMsg ItemEdit.Msg
    | ViewItemMsg VI.Msg
    | DoViewItem PlanItem.Model
    | ViewItemDone
    | DoAddItem
    | DoDeleteItem Int
    | ItemAdded (Maybe PlanItem.Model)

itemEditCMC : ItemEdit.Msg -> Msg
itemEditCMC msg =
    case msg of
        ItemEdit.FpDone mbItem -> ItemAdded mbItem
        ItemEdit.FpError errMsg -> FpError errMsg
        _ -> ItemEditMsg msg

viewItemCMC : VI.Msg -> Msg
viewItemCMC msg =
    case msg of
        VI.FpDone Nothing -> ViewItemDone
        VI.FpDone (Just pItem) -> ViewItemDone
        VI.FpError errMsg -> FpError errMsg
        _ -> ViewItemMsg msg

update : GlobalContext -> Msg -> Model -> (Maybe Model, Cmd Msg)
update gc msg model =
    case msg of
        FpError _ ->
            Debug.crash "This mesage should have been intercepted by the parent!"
        FpBack ->
            Debug.crash "This mesage should have been intercepted by the parent!"
        FpShowItems subMsg ->
            Debug.crash "This mesage should have been intercepted by the parent!"
        FetchDone (Err errMsg) ->
            (Just model, Task.succeed (toString errMsg) |> Task.perform FpError)
        FetchDone (Ok items) ->
            (Just {model|items = items}, Cmd.none)
        DoAddItem ->
            (Just {model|activeComp = ItemEditComp}, Cmd.none)
        DoDeleteItem itemId ->
            (Just model, PlanItem.delete itemId DeleteDone )
        DeleteDone (Err errMsg) ->
            (Just model, Task.succeed (toString errMsg) |> Task.perform FpError)
        DeleteDone (Ok itemId) ->
            let newItems = List.filter (\item -> item.id /= itemId) model.items
            in (Just {model|items = newItems}, Cmd.none)
        ItemAdded mbItem->
            case mbItem of
                Nothing -> (Just {model|activeComp = Me}, Cmd.none)
                Just item ->
                    let newItems = item :: model.items
                    in (Just {model|activeComp = Me, items = newItems}, Cmd.none)
        ItemEditMsg subMsg ->
            let
                (subModel, cmd) = ItemEdit.update gc subMsg model.itemEditCM
            in
                (Just {model|itemEditCM = subModel}, Cmd.map itemEditCMC cmd)
        DoViewItem item ->
            ( Just {model| viewItemCM = Just <| VI.initialModel gc item.attributes}
            , VI.activate gc.pItems item |> Cmd.map viewItemCMC
            )
        ViewItemDone ->
            (Just {model|viewItemCM = Nothing}, Cmd.none)
        ViewItemMsg subMsg ->
            case model.viewItemCM of
                Nothing -> (Just model, Cmd.none)
                Just subModel ->
                    let
                        (newSubModel, cmd) = VI.update gc subMsg subModel
                    in
                        (Just {model|viewItemCM = Just newSubModel}, Cmd.map viewItemCMC cmd)

itemsView : GC.GlobalContext -> Model -> Html Msg
itemsView gc model =
    let
        itemType = model.itemType
        items = model.items
    in
        if model.activeComp == Me then
            case model.viewItemCM of
                Nothing ->
                    div []
                        [ h1 [] [ text ("Group summmary: " ++ itemType.name)]
                        , button [ onClick FpBack ] [ text "Close" ]
                        , button [ onClick <| FpShowItems itemType ] [ text "Items" ]
                        , button [ onClick DoAddItem ] [ text "Add" ]
                        , table []
                            [ thead []
                                [ tr [] (
                                    List.concat
                                        [
                                            if itemType.attr1 /= "" then
                                                [ th [] [ text itemType.attr1 ] ]
                                            else []
                                        ,
                                            if itemType.attr2 /= "" then
                                                [ th [] [ text itemType.attr2 ] ]
                                            else []
                                        ,
                                            if itemType.attr3 /= "" then
                                                [ th [] [ text itemType.attr3 ] ]
                                            else []
                                        ,
                                            if itemType.attr4 /= "" then
                                                [ th [] [ text itemType.attr4 ] ]
                                            else []
                                        ,
                                            if itemType.attr5 /= "" then
                                                [ th [] [ text itemType.attr5 ] ]
                                            else []
                                        , [ th [] [ text "Analogs#"] ]
                                        , [ th [] [ text "Boms#"] ]
                                        , [ th [] [ text "Main item description"] ]
                                        , [ th [] [ text "Action"] ]
                                        ])
                                ]
                            , tbody []
                                (List.map (\item -> itemRowView itemType item) items)
                            ]
                        ]
                Just subModel ->
                    Html.map viewItemCMC (VI.compView gc subModel)
        else
            Html.map itemEditCMC <| ItemEdit.itemEditView gc model.itemEditCM

itemRowView : ItemType -> PlanItem.Model -> Html Msg
itemRowView itemType item =
    tr [onDoubleClick <| DoViewItem item] (
        List.concat 
            [
                if itemType.attr1 /= "" then
                    [ td [][text item.attributes.attr1]]
                else []
            ,
                if itemType.attr2 /= "" then
                    [ td [][text item.attributes.attr2]]
                else []
            ,
                if itemType.attr3 /= "" then
                    [ td [][text item.attributes.attr3]]
                else []
            ,
                if itemType.attr4 /= "" then
                    [ td [][text item.attributes.attr4]]
                else []
            ,
                if itemType.attr5 /= "" then
                    [ td [][text item.attributes.attr5]]
                else []
            ,   [ td [][text <| toString item.analogQty]]
            ,   [ td [][text <| toString <| item.bomsQty - item.analogQty]]
            ,   [ td [][text item.mainItemDescr ]]
            ,   [ td [][button [onClick <| DoDeleteItem item.id][ text "x"]]]
            ])
        
