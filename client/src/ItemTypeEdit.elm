module ItemTypeEdit exposing (..)

import Task
import Json.Encode as Encode
import Json.Decode as Decode
import Http
import Html exposing (..)
import Html.Events exposing (onClick, onInput, onCheck)
import Html.Attributes exposing (type_, class, value, disabled, selected, checked)
import Helper
import GlobalContext as GC exposing (..)


type alias Model =
    { itemType : GC.ItemType
    }

initialModel : Model
initialModel =
    { itemType = GC.emptyType
    }

type Msg
    = FpTypeSaved GC.ItemType
    | FpError String
    | Activate (Maybe GC.ItemType)
    | Save
    | SaveDone (Result Http.Error Int)
    | Back
    | FpBack
    | GroupSetTo String
    | NameSetTo String
    | UomSetTo String
    | RecuperationSetTo String
    | PlanningMethodSetTo String
    | LocalPurchaseSetTo Bool
    | CustomsCodeSetTo String
    | CustomsDutySetTo String 
    | TemplateSetTo String
    | Attr1SetTo String
    | Attr2SetTo String
    | Attr3SetTo String
    | Attr4SetTo String
    | Attr5SetTo String
    | Values1SetTo String
    | Values2SetTo String
    | Values3SetTo String
    | Values4SetTo String
    | Values5SetTo String
    | NoteSetTo String


saveUrl : String
saveUrl = "/api/types"

encodeItemType : GC.ItemType -> Encode.Value
encodeItemType it =
    Encode.object
        [ ("id", Encode.int it.id)
        , ("group", Encode.string it.group)
        , ("name", Encode.string it.name)
        , ("uom", Encode.string it.uom)
        , ("recuperation", Encode.float it.recuperation)
        , ("planningMethod", Encode.string it.planningMethod)
        , ("localPurchase", Encode.bool it.localPurchase)
        , ("customsCode", Encode.string it.customsCode)
        , ("customsDuty", Encode.float it.customsDuty)
        , ("descrTmpl", Encode.string it.descrTmpl)
        , ("attr1Descr", Encode.string it.attr1)
        , ("attr1Values", Encode.string it.attr1Values)
        , ("attr2Descr", Encode.string it.attr2)
        , ("attr2Values", Encode.string it.attr2Values)
        , ("attr3Descr", Encode.string it.attr3)
        , ("attr3Values", Encode.string it.attr3Values)
        , ("attr4Descr", Encode.string it.attr4)
        , ("attr4Values", Encode.string it.attr4Values)
        , ("attr5Descr", Encode.string it.attr5)
        , ("attr5Values", Encode.string it.attr5Values)
        , ("note", Encode.string it.note)
        ]

save : GC.ItemType -> Cmd Msg
save itype =
    case itype.id of
        0 -> Http.send SaveDone (Http.post saveUrl (Http.jsonBody <| encodeItemType itype) Decode.int) 
        id -> Http.send SaveDone (Helper.httpPut saveUrl id (Http.jsonBody <| encodeItemType itype) Decode.int) 


update : GC.GlobalContext -> Msg -> Model -> (Model, Cmd Msg)
update gc msg model =
    let itype = model.itemType 
    in case msg of
        FpTypeSaved itype ->
            Debug.crash "This message should have been intercepted by parent."
        FpBack ->
            Debug.crash "This message should have been intercepted by parent."
        FpError _ ->
            Debug.crash "This message should have been intercepted by parent."
        Activate mItemType ->
            let itype =
                case mItemType of
                    Nothing -> GC.emptyType
                    Just itype -> itype
            in ({model|itemType=itype}, Cmd.none)
        Save ->
            (model, save model.itemType)
        SaveDone (Err errMsg) ->
            (model, Task.succeed (toString errMsg) |> Task.perform FpError)
        SaveDone (Ok id) ->
            let itype = model.itemType
                savedType = {itype|id = id}
            in (initialModel, Task.perform (\a->FpTypeSaved a) (Task.succeed savedType))
        Back ->
            (model, Task.perform (\a->FpBack) (Task.succeed never))
        GroupSetTo newData ->
           let newItype = {itype|group = newData}
           in ({model|itemType = newItype}, Cmd.none)
        NameSetTo newData ->
           let newItype = {itype|name = newData}
           in ({model|itemType = newItype}, Cmd.none)
        UomSetTo newData ->
           let newItype = {itype|uom = newData}
           in ({model|itemType = newItype}, Cmd.none)
        RecuperationSetTo newData ->
           let
               newRecuperation = Result.withDefault 0 (String.toFloat newData)
               newItype = {itype|recuperation = newRecuperation}
           in
               ({model|itemType = newItype}, Cmd.none)
        PlanningMethodSetTo newData ->
           let newItype = {itype|planningMethod = newData}
           in ({model|itemType = newItype}, Cmd.none)
        LocalPurchaseSetTo newData ->
           let newItype = {itype|localPurchase = newData}
           in ({model|itemType = newItype}, Cmd.none)
        CustomsCodeSetTo newData ->
           let newItype = {itype|customsCode = newData}
           in ({model|itemType = newItype}, Cmd.none)
        CustomsDutySetTo newData ->
           let
               newDuty = Result.withDefault 0 (String.toFloat newData)
               newItype = {itype|customsDuty = newDuty}
           in
               ({model|itemType = newItype}, Cmd.none)
        TemplateSetTo newData ->
           let newItype = {itype|descrTmpl = newData}
           in ({model|itemType = newItype}, Cmd.none)
        Attr1SetTo newData ->
           let newItype = {itype|attr1 = newData}
           in ({model|itemType = newItype}, Cmd.none)
        Attr2SetTo newData ->
           let newItype = {itype|attr2 = newData}
           in ({model|itemType = newItype}, Cmd.none)
        Attr3SetTo newData ->
           let newItype = {itype|attr3 = newData}
           in ({model|itemType = newItype}, Cmd.none)
        Attr4SetTo newData ->
           let newItype = {itype|attr4 = newData}
           in ({model|itemType = newItype}, Cmd.none)
        Attr5SetTo newData ->
           let newItype = {itype|attr5 = newData}
           in ({model|itemType = newItype}, Cmd.none)
        Values1SetTo newData ->
           let newItype = {itype|attr1Values = newData}
           in ({model|itemType = newItype}, Cmd.none)
        Values2SetTo newData ->
           let newItype = {itype|attr2Values = newData}
           in ({model|itemType = newItype}, Cmd.none)
        Values3SetTo newData ->
           let newItype = {itype|attr3Values = newData}
           in ({model|itemType = newItype}, Cmd.none)
        Values4SetTo newData ->
           let newItype = {itype|attr4Values = newData}
           in ({model|itemType = newItype}, Cmd.none)
        Values5SetTo newData ->
           let newItype = {itype|attr5Values = newData}
           in ({model|itemType = newItype}, Cmd.none)
        NoteSetTo newData ->
           let newItype = {itype|note = newData}
           in ({model|itemType = newItype}, Cmd.none)

isModelValid : ItemType -> Bool
isModelValid itype =
    (String.length itype.group) > 0 && (String.length itype.name > 0)

compView : GlobalContext -> Model -> Html Msg
compView gc model =
    div [class "form" ]
        [ h2 [] [ text "Add/Edit Item Type" ]
        , div [ class "formBody" ]
            [ div [ class "labelGroup" ]
                [ label [] [ text "Group" ]
                , div []
                    [ select
                        [onInput GroupSetTo, value model.itemType.group]
                        (option [][] :: (List.map (\g -> option [ selected (g == model.itemType.group)] [text g]) gc.itemGroups))
                    ]
                ]
            , div [ class "labelGroup" ]
                [ label [] [ text "Name" ]
                , div []
                   [ input [onInput NameSetTo, value model.itemType.name][]
                   ]
                ]
            , div [ class "labelGroup" ]
                [ label [] [ text "UOM" ]
                , div []
                    [ select
                        [onInput UomSetTo, value model.itemType.uom]
                        (option [][] :: (List.map (\uom -> option [ selected (uom == model.itemType.uom)] [text uom]) gc.uoms))
                    ]
                ]
            , div [ class "labelGroup" ]
                [ label [] [ text "Recuperation" ]
                , div []
                    [ input [onInput RecuperationSetTo, value <| toString model.itemType.recuperation, type_ "number"][]
                    ]
                ]
            , div [ class "labelGroup" ]
                [ label [] [ text "Planning method" ]
                , div []
                    [ select
                        [onInput PlanningMethodSetTo, value model.itemType.planningMethod]
                        (option [][] :: (List.map (\pm -> option [ selected (pm == model.itemType.planningMethod)] [text pm]) gc.planningMethods))
                    ]
                ]
            , div [ class "labelGroup" ]
                [ label [] [ text "Local purchase?" ]
                , div []
                    [ input [onCheck LocalPurchaseSetTo, type_ "checkbox", checked model.itemType.localPurchase][]
                    ]
                ]
            , div [ class "labelGroup" ]
                [ label [] [ text "Customs code" ]
                , div []
                    [ input [onInput CustomsCodeSetTo, value model.itemType.customsCode][]
                    ]
                ]
            , div [ class "labelGroup" ]
                [ label [] [ text "Customs duty" ]
                , div []
                    [ input [onInput CustomsDutySetTo, value <| toString model.itemType.customsDuty, type_ "number"][]
                    ]
                ]
            , div [ class "labelGroup" ]
                [ label [] [ text "Note" ]
                , div []
                    [ input [onInput NoteSetTo, value model.itemType.note][]
                    ]
                ]
            , div [ class "labelGroup" ]
                [ label [] [ text "Items template" ]
                , div []
                    [ input [onInput TemplateSetTo, value model.itemType.descrTmpl][]
                    ]
                ]
            , div [ class "subFormNarrow" ]
                [ div [ class "labelGroup" ]
                    [ label [] [ text "Attr1" ]
                    , div []
                        [ input [onInput Attr1SetTo, value model.itemType.attr1][]
                        ]
                    ]
                , div [ class "labelGroup" ]
                    [ label [] [ text "Attr2" ]
                    , div []
                        [ input [onInput Attr2SetTo, value model.itemType.attr2][]
                        ]
                    ]
                , div [ class "labelGroup" ]
                    [ label [] [ text "Attr3" ]
                    , div []
                        [ input [onInput Attr3SetTo, value model.itemType.attr3][]
                        ]
                    ]
                , div [ class "labelGroup" ]
                    [ label [] [ text "Attr4" ]
                    , div []
                        [ input [onInput Attr4SetTo, value model.itemType.attr4][]
                        ]
                    ]
                , div [ class "labelGroup" ]
                    [ label [] [ text "Attr5" ]
                    , div []
                        [ input [onInput Attr5SetTo, value model.itemType.attr5][]
                        ]
                    ]
                ]
            , div [ class "subFormWide" ]
                [ div [ class "labelGroup" ]
                    [ label [] [ text "Values" ]
                    , div []
                        [ input [onInput Values1SetTo, value model.itemType.attr1Values][]
                        ]
                    ]
                , div [ class "labelGroup" ]
                    [ label [] [ text "Values" ]
                    , div []
                        [ input [onInput Values2SetTo, value model.itemType.attr2Values][]
                        ]
                    ]
                , div [ class "labelGroup" ]
                    [ label [] [ text "Values" ]
                    , div []
                        [ input [onInput Values3SetTo, value model.itemType.attr3Values][]
                        ]
                    ]
                , div [ class "labelGroup" ]
                    [ label [] [ text "Values" ]
                    , div []
                        [ input [onInput Values4SetTo, value model.itemType.attr4Values][]
                        ]
                    ]
                , div [ class "labelGroup" ]
                    [ label [] [ text "Values" ]
                    , div []
                        [ input [onInput Values5SetTo, value model.itemType.attr5Values][]
                        ]
                    ]
                ]
            , div [ class "formLineButtons" ]
                [ button [onClick Back][text "Back"] 
                , button [onClick Save, disabled <| not (isModelValid model.itemType)][text "Save"] 
                ]
            ]
        ]
