module Boms.AddPlanItem exposing (..)

import Task
import Json.Encode as Encode
import Json.Decode as Decode exposing (field, index, fail, Decoder)
import Http
import Html exposing (..)
import Html.Events exposing (onClick, onDoubleClick, onInput)
import Html.Attributes as A exposing (..)

import Helper exposing (..)
import GlobalContext as GC exposing (..)
import Models as M exposing (..)
import PlanItems.Model as PlanItem exposing (..)
import Boms as Boms exposing (..)

type Msg
    = FetchPlanItemsDone (Result Http.Error (List PlanItem.Model))
    | SaveBomDone (Result Http.Error Boms.Bom)
    | GroupSelectorChangedTo String
    | TypeSelectorChangedTo String
    | BomPartSetTo String
    | BomQtySetTo String
    | DoAddBom
    | FpError String
    | FpBOMadded Boms.Bom 


saveBom : Bom -> Cmd Msg
saveBom bom =
    Http.send SaveBomDone (Http.post Boms.bomUrl (Http.jsonBody <| Boms.bomEncoder bom) Boms.bomDecoder) 


type alias Model =
    { item : M.Item
    , groupSelector: String
    , typeSelector: Int
    , planItems : List PlanItem.Model
    , newBomPart : Maybe PlanItem.Model
    , newBomQty : Float
    }

initialModel : M.Item -> Model
initialModel item =
    { item = item
    , groupSelector = ""
    , typeSelector = 0 
    , planItems = []
    , newBomPart = Nothing
    , newBomQty = 0
    }

update : GC.GlobalContext -> Msg -> Model -> (Model, Cmd Msg)
update gc msg model =
    case msg of
        FetchPlanItemsDone (Err errMsg) ->
            (model, Task.succeed (toString errMsg) |> Task.perform FpError)
        FetchPlanItemsDone (Ok newPlanItems) ->
            ({model|planItems = newPlanItems}, Cmd.none)
        GroupSelectorChangedTo newGroup ->
            (,)
                {model
                | groupSelector = newGroup
                , typeSelector = 0
                , planItems = []
                , newBomPart = Nothing
                }
                Cmd.none
        TypeSelectorChangedTo newType ->
                let
                    newTypeId = Result.withDefault 0 (String.toInt newType)
                in
                    (,)
                        {model
                        | typeSelector = newTypeId
                        , planItems = []
                        , newBomPart = Nothing
                        }
                        <| PlanItem.fetch newTypeId FetchPlanItemsDone
        BomPartSetTo partIdStr ->
            let partId = Result.withDefault 0 (String.toInt partIdStr)
                part = List.head <| List.filter (\part -> part.id == partId) model.planItems
            in case part of
                Nothing -> (model, Cmd.none)
                Just planItem -> ({model | newBomPart = Just planItem}, Cmd.none)

        BomQtySetTo qtyStr ->
            let qty = Result.withDefault 0 (String.toFloat qtyStr)
            in ({model| newBomQty=qty}, Cmd.none)
        DoAddBom ->
            case (prepareBom 0 model) of
                Just bom -> (model, saveBom bom)
                Nothing -> (model, Cmd.none)
        SaveBomDone (Ok bom) ->
            (model, Task.succeed bom |> Task.perform FpBOMadded)
        SaveBomDone (Err errMsg) ->
            (model, Task.succeed (toString errMsg) |> Task.perform FpError)
        FpError _ -> 
            Debug.crash <| "This message should have been intercepted by my parent!"
        FpBOMadded _ -> 
            Debug.crash <| "This message should have been intercepted by my parent!"

prepareBom : Int -> Model -> Maybe Bom
prepareBom id model =
    case model.newBomPart of
        Just part ->
            if model.newBomQty > 0 then
                Just <| Bom id model.item part.attributes model.newBomQty False
            else
                Nothing
        Nothing -> Nothing

validate : Model -> Bool
validate model =
    model.newBomPart /= Nothing &&
    model.newBomQty > 0 

--##############################################################################################
newPlanItem : GC.GlobalContext -> Model -> Html Msg
newPlanItem gc model =
    div [ class "form" ]
        [ h2 []
            [ text "Connect with planning item"
            ]
        , div [ A.class "formBody" ]
            [ div [ A.class "labelGroup" ]
                [ label [] [ text "Group" ] 
                , div []
                    [ select [onInput GroupSelectorChangedTo ]
                        ((option [][text ""]) ::
                            (List.map
                                (\igroup ->
                                    option [A.value igroup, A.selected (igroup == model.groupSelector) ] [text igroup])
                                gc.itemGroups))
                    ]
                ]
            , div [ A.class "labelGroup" ]
                [ label [] [ text "Type" ] 
                , div []
                    [ select  [onInput TypeSelectorChangedTo ]
                        ((option [][text ""]) ::
                        (List.map
                            (\itype ->
                                option [value <| toString itype.id, selected (itype.id == model.typeSelector)] [text itype.name])
                            (List.filter (\itype -> itype.group == model.groupSelector ) gc.itemTypes)))
                    ]
                ]
            , div [ A.class "labelGroup" ]
                [ label [] [ text "Planning item" ]
                , div []
                    [ select [onInput BomPartSetTo ]
                        ((option [][text ""]) ::
                        ( List.map
                            (\planItem ->
                                option
                                    [ A.value <| toString planItem.id
                                    , A.selected <|
                                        planItem.id == ( 
                                            Maybe.withDefault
                                                0 <|
                                                Maybe.map (\part -> part.id) model.newBomPart)
                                    ]
                                    [text <| PlanItem.description gc.itemTypes planItem.attributes ])
                            model.planItems))
                    ]
                ]
            , div [ A.class "labelGroup" ]
                [ label [] [ text "Qty" ]
                , div []
                    [ input [onInput BomQtySetTo, A.value <| toString model.newBomQty, A.type_ "number"][]
                    ]
                ]
            , button [ onClick DoAddBom, A.disabled <| not (validate model) ] [text "Add"]
            ]
        ] 

