module ViewPlanItem exposing (..)

import Task
import Http
import Html exposing (..)
import Html.Events exposing (onClick, onInput)
import Html.Attributes exposing (type_, value, selected, disabled)

import Models exposing (PlanItem, Item)
import GlobalContext as GC exposing (..)
import Boms exposing (..)
import EditPlanItem as EPI exposing (..)
import Items exposing (..)
import Helper as H exposing (..)

type Msg
    = FpDone (Maybe Models.PlanItem)
    | FpError String
    | DoEditItem
    | DoSearchItem
    | EditPlanItemCompMsg EPI.Msg
    | BomsCompMsg Boms.Msg
    | ItemsCompMsg Items.Msg
    | ItemSelected (Maybe Item)

editBomCompMsgConv : Boms.Msg -> Msg
editBomCompMsgConv msg =
   case msg of
       Boms.FpDoSearchItem -> DoSearchItem
       Boms.FpError errMsg -> FpError errMsg
       _ -> BomsCompMsg msg

editPlanItemCompMsgConv : EPI.Msg -> Msg
editPlanItemCompMsgConv msg =
    case msg of
        EPI.FpDone _ -> FpDone Nothing 
        EPI.FpError errMsg -> FpError errMsg
        _ -> EditPlanItemCompMsg msg

itemsCompMsgConv : Items.Msg -> Msg
itemsCompMsgConv msg =
    case msg of
        Items.FpDone subMsg -> ItemSelected subMsg
        _ -> ItemsCompMsg msg


type alias Model =
    { planItem : Models.PlanItem
    , editBomCompModel : Boms.Model
    , editPlanItemCompModel : EPI.Model
    , itemsCompModel : Items.Model
    , isItemsCompActive : Bool
    }


initialModel : GC.GlobalContext -> Models.PlanItem -> Model
initialModel gc item =
    { planItem = item
    , editBomCompModel = Boms.setPart (Just item) Boms.initialModel
    , editPlanItemCompModel = EPI.initialModel gc (Just item)
    , itemsCompModel = Items.initialModel
    , isItemsCompActive = False
    }

 
activate : List Models.PlanItem -> Models.PlanItem -> Cmd Msg
activate pItems item =
    Cmd.batch
        [ Cmd.map editPlanItemCompMsgConv <| EPI.fetchAttributes item
        , Cmd.map editBomCompMsgConv <| Boms.fetchBomByPart pItems item.id
        ]

update : GC.GlobalContext -> Msg -> Model -> (Model, Cmd Msg)
update gc msg model =
    case msg of
        FpDone errMsg ->
            Debug.crash "This message should have been intercepted by the parent"
        FpError errMsg ->
            Debug.crash "This message should have been intercepted by the parent"

        ItemSelected mItem ->
            let
                newBomsCompModel = Boms.setItem mItem model.editBomCompModel
            in
                (,)
                    {model
                    | isItemsCompActive = False
                    , editBomCompModel = newBomsCompModel
                    }
                    Cmd.none

        DoEditItem ->
            (model, Cmd.none)

        EditPlanItemCompMsg subMsg ->
            let (newModel, cmd) = EPI.update gc subMsg model.editPlanItemCompModel
            in ({model|editPlanItemCompModel = newModel}, Cmd.map editPlanItemCompMsgConv cmd)

        BomsCompMsg subMsg ->
            let (newModel, cmd) = Boms.update gc subMsg model.editBomCompModel
            in ({model|editBomCompModel = newModel}, Cmd.map editBomCompMsgConv cmd)

        ItemsCompMsg subMsg ->
                let (newModel, cmd) = Items.update gc subMsg model.itemsCompModel
                in ({model|itemsCompModel = newModel}, Cmd.map itemsCompMsgConv cmd)
        DoSearchItem ->
            ({model|isItemsCompActive = True}, Cmd.none)


compView : GC.GlobalContext -> Model -> Html Msg
compView gc model =
    if model.isItemsCompActive then
        div []
            [ h1[] [ text model.planItem.piDescr ]
            , Html.map itemsCompMsgConv (Items.selectView  model.itemsCompModel)
            ]
    else
        div []
            [ Html.map editPlanItemCompMsgConv <| EPI.itemEditView gc model.editPlanItemCompModel
--            , Html.map editPlanItemCompMsgConv <| EPI.itemView gc model.editPlanItemCompModel
            , h3 [][text "This item is connected with"]
            , Html.map editBomCompMsgConv <| Boms.viewBomItems model.editBomCompModel
            , Html.map editBomCompMsgConv <| Boms.newItem model.editBomCompModel
            ]
