module Boms exposing (..)

import Task
import Json.Encode as Encode
import Json.Decode as Decode exposing (field, index, fail, Decoder)
import Http
import Html exposing (..)
import Html.Events exposing (onClick, onDoubleClick, onInput)
import Html.Attributes as A exposing (..)

import Helper exposing (..)
import GlobalContext as GC exposing (..)
import Models as M exposing (..)
import PlanItems.Model as PlanItem exposing (..)

type Msg
    = Activate M.Item
    | FetchBomDone (Result Http.Error (List Bom))
    | SaveBomDone (Result Http.Error Int)
    | DeleteBomDone (Result Http.Error Int)
    | UpdateBomDone (Result Http.Error Int)
    | GroupSelectorChangedTo String
    | TypeSelectorChangedTo String
    | BomPartSetTo String
    | BomQtySetTo String
    | DoDeleteBom Int
    | DoAddBom
    | SetMainItem Bom
    | FpDoSearchItem
    | FpError String

type alias Bom =
    { id : Int
    , item : M.Item
    , part : PlanItem.Attributes
    , qty : Float
    , isMain : Bool
    }

bomUrl : String
bomUrl = "/api/boms"

fetchBom : String -> Cmd Msg
fetchBom url  = Http.send FetchBomDone (Http.get url listBomsDecoder)

fetchBomByItem : Int -> Cmd Msg
fetchBomByItem id = bomUrl ++ "/byitem/" ++ (toString id) |> fetchBom

fetchBomByPart : Int -> Cmd Msg
fetchBomByPart id = bomUrl ++ "/bypart/" ++ (toString id) |> fetchBom

delete : Int -> Cmd Msg
delete id = Http.send DeleteBomDone (Helper.httpDelete bomUrl id)

saveBom : Bom -> Cmd Msg
saveBom bom =
    Http.send SaveBomDone (Http.post bomUrl (Http.jsonBody <| bomEncoder bom) Decode.int) 

updateBom : Bom -> Cmd Msg
updateBom bom =
    Http.send UpdateBomDone (Helper.httpPut bomUrl bom.id (Http.jsonBody <| bomEncoder bom) Decode.int) 

listBomsDecoder : Decode.Decoder (List Bom)
listBomsDecoder = Decode.list bomDecoder


bomDecoder : Decode.Decoder Bom
bomDecoder =
    let 
        idDec = (field "id" Decode.int)
        qtyDec = (field "qty" Decode.float)
    in
        Decode.map5
            Bom
            (index 0 idDec)
            (index 1 M.itemDecoder)
            (index 2 PlanItem.attributesDecoder)
            (index 0 qtyDec)
            (index 0 (field "isMain" Decode.bool))

bomEncoder : Bom -> Encode.Value
bomEncoder bom =
    Encode.object
        [ ("item", Encode.int bom.item.id)
        , ("pitem", Encode.int bom.part.id)
        , ("qty", Encode.float bom.qty)
        , ("isMain", Encode.bool bom.isMain)
        ]

type alias Model =
    { item : Maybe M.Item
    , groupSelector: String
    , typeSelector: Int
    , boms : List Bom
    , newBomPart : Maybe PlanItem.Attributes
    , newBomQty : Float
    }

initialModel : Model
initialModel =
    { item = Nothing
    , groupSelector = ""
    , typeSelector = 0 
    , boms = []
    , newBomPart = Nothing
    , newBomQty = 0
    }

setItem : Maybe M.Item -> Model -> Model
setItem item model = {model|item=item}

setPart : Maybe PlanItem.Attributes -> Model -> Model
setPart mPart model = {model|newBomPart=mPart}

addBom : Bom -> Model -> Model
addBom bom model =
    let newBoms = bom :: model.boms
    in {model | boms = newBoms}

update : GC.GlobalContext -> Msg -> Model -> (Model, Cmd Msg)
update gc msg model =
    case msg of
        Activate newItem ->
            (,)
                {model
                | item = Just newItem
                , boms = []
                }
                (fetchBomByItem newItem.id)
        FetchBomDone (Err errMsg) ->
            (model, Task.succeed (toString errMsg) |> Task.perform FpError)
        FetchBomDone (Ok newBoms) ->
            ({model|boms = newBoms}, Cmd.none)
        DoDeleteBom id ->
            (model, delete id)
        DeleteBomDone (Err errMsg) ->
            (model, Task.succeed (toString errMsg) |> Task.perform FpError)
        DeleteBomDone (Ok bomId) ->
            let newBoms = List.filter (\bom -> bom.id /= bomId) model.boms
            in ({model| boms = newBoms}, Cmd.none)
        UpdateBomDone (Err errMsg) ->
            (model, Task.succeed (toString errMsg) |> Task.perform FpError)
        UpdateBomDone (Ok bomId) ->
            let
                newBoms = List.map (\bom -> {bom|isMain = (bom.id == bomId)}) model.boms
            in
                ({model|boms = newBoms}, Cmd.none)
            
        GroupSelectorChangedTo newGroup ->
            (,)
                {model
                | groupSelector = newGroup
                , typeSelector = 0
                , newBomPart = Nothing
                }
                Cmd.none
        TypeSelectorChangedTo newType ->
                let
                    newTypeId = Result.withDefault 0 (String.toInt newType)
                in
                    (,)
                        {model
                        | typeSelector = newTypeId
                        , newBomPart = Nothing
                        }
                        Cmd.none
        BomPartSetTo idStr ->
            (model, Cmd.none)
        BomQtySetTo qtyStr ->
            let qty = Result.withDefault 0 (String.toFloat qtyStr)
            in ({model| newBomQty=qty}, Cmd.none)
        DoAddBom ->
            case (prepareBom 0 model) of
                Just bom -> (model, saveBom bom)
                Nothing -> (model, Cmd.none)
        SaveBomDone (Ok bomId) ->
            let newBoms =
                case (prepareBom bomId model) of
                    Nothing -> model.boms
                    Just newBom -> newBom :: model.boms
            in ({model|boms=newBoms}, Cmd.none)
        SaveBomDone (Err errMsg) ->
            (model, Task.succeed (toString errMsg) |> Task.perform FpError)
        SetMainItem bom ->
            (model, updateBom bom)
        FpDoSearchItem ->
            Debug.crash <| "This message should have been intercepted by my parent!"
        FpError _ -> 
            Debug.crash <| "This message should have been intercepted by my parent!"

prepareBom : Int -> Model -> Maybe Bom
prepareBom id model =
    case model.item of
        Just item ->
            case model.newBomPart of
                Just part ->
                    if model.newBomQty > 0 then
                        Just <| Bom id item part model.newBomQty False
                    else
                        Nothing
                Nothing -> Nothing
        Nothing -> Nothing

validate : Model -> Bool
validate model =
    model.item /= Nothing &&
    model.newBomPart /= Nothing &&
    model.newBomQty > 0 


--#######################################################################33
newItem : Model -> Html Msg
newItem model =
    let
        itemSelected =
            case model.item of
                Nothing -> ""
                Just item -> (toString item.code1C) ++ " " ++ item.description
    in
        div [ A.class "form" ]
            [ h2 [] [ text "Connect with item"]
            , div [ A.class "formBody" ]
                [ div [ A.class "labelGroup" ]
                    [ label [] [ text "Item"]
                    , div []
                        [ input [ onDoubleClick FpDoSearchItem, A.value itemSelected][] 
                        ]
                    ]
                , div [ A.class "labelGroup" ]
                    [ label [] [ text "Qty"] 
                    , div []
                        [ input [onInput BomQtySetTo, A.value <| toString model.newBomQty, A.type_ "number"] []
                        ]
                    ]
                , div [ A.class "formLineButtons" ]
                    [ div []
                        [ button [ onClick DoAddBom ] [text "Add BOM"]
                        ]
                    ]
                ]
            ]
--##############################################################################################
viewBomPlanItems : List GC.ItemType -> Model -> Html Msg
viewBomPlanItems itypes model =
    table []
        [ thead []
            [ tr []
                [ th [] [text "Description"]
                , th [] [text "Qty"]
                , th [] [text "Action"]
                ]
            ]
        , tbody []
            (List.map (viewBomPlanItem itypes) model.boms)
        ]

viewBomPlanItem : List GC.ItemType -> Bom -> Html Msg
viewBomPlanItem itypes bom =
    tr []
        [ td [] [text <| PlanItem.description itypes bom.part]
        , td [] [text <| toString bom.qty]
        , td [] [button [onClick <| DoDeleteBom bom.id] [text "x"]]
        ]

--#######################################################################33
viewBomItems : Model -> Html Msg
viewBomItems model =
    table []
        [ thead []
            [ tr []
                [ th [] [text "Code1C"]
                , th [] [text "PN"]
                , th [] [text "Description"]
                , th [] [text "UOM"]
                , th [] [text "Qty"]
                , th [] [text "Action"]
                ]
            ]
        , tbody []
            (List.map viewBomItem model.boms)
        ]

viewBomItem : Bom -> Html Msg
viewBomItem bom =
    tr [ SetMainItem bom |> onDoubleClick, A.classList [("selected", bom.isMain) ]]
        [ td [] [text <| toString bom.item.code1C]
        , td [] [text bom.item.pn]
        , td [] [text bom.item.description]
        , td [] [text bom.item.uom]
        , td [] [text <| toString bom.qty]
        , td [] [button [onClick <| DoDeleteBom bom.id] [text "x"]]
        ]

