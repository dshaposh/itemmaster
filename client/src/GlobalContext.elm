module GlobalContext exposing (..)
import Set exposing (..)

import Models as M exposing (..)

type alias ItemType =
    { id :          Int
    , name :        String
    , group :       String
    , uom :         String
    , recuperation: Float
    , planningMethod: String
    , localPurchase: Bool
    , customsCode : String
    , customsDuty : Float
    , descrTmpl:    String
    , attr1 :       String
    , attr1Values : String
    , attr2 :       String
    , attr2Values : String
    , attr3 :       String
    , attr3Values : String
    , attr4 :       String
    , attr4Values : String
    , attr5 :       String
    , attr5Values : String
    , note :        String
    , itemsQty :    Int
    , pItemsQty :   Int
    }

emptyType : ItemType
emptyType = ItemType 0 "" "" "" 0 "" False "" 0 "" "" "" "" "" "" "" "" "" "" "" "" 0 0

type alias GlobalContext =
    { itemGroups : List String
    , uoms : List String
    , planningMethods : List String
    , itemTypes : List ItemType
    , pItems : List M.PlanItem
    }

initialData : GlobalContext
initialData =
    { itemGroups =
        [ "Pump"
        , "Pump-parts"
        , "GS/Intake"
        , "GS/Intake-parts"
        , "Seal"
        , "Seal-parts"
        , "Motor"
        , "Motor-parts"
        , "Accessories"
        , "Consumables"
        , "Cable"
        , "Sensor"
        , "SF"
        , "Tools"
        , "Other"
        ]
    , uoms = ["ea", "m", "mm", "ft", "gl", "kg"]
    , planningMethods = ["By forecast", "By history", "No planning"]
    , itemTypes = []
    , pItems = []
    }
