module ViewItem exposing (..)

import Task
import Json.Encode as Encode
import Json.Decode as Decode exposing (field, index, fail, Decoder)
import Http
import Html exposing (..)
import Html.Events exposing (onClick, onInput)
import Html.Attributes exposing (class, value, type_, selected, disabled)

import Helper exposing (..)
import GlobalContext as GC exposing (..)
import Models as M exposing (..)

import Boms exposing (..)
import PlanItems.Model as PlanItem exposing (..)
import Boms.AddPlanItem as AddPlanItem exposing (..)

type Msg
    = Activate M.Item
    | BomsMsg Boms.Msg
    | AddBOMcompMsg AddPlanItem.Msg
    | FpClose
    | DoEdit M.Item
    | FpError String
    | BOMadded Boms.Bom

bomsCompMsgConv : Boms.Msg -> Msg
bomsCompMsgConv msg =
    case msg of
        Boms.FpError errMsg -> FpError errMsg
        _ -> BomsMsg msg

addBOMcompMsgConv : AddPlanItem.Msg -> Msg
addBOMcompMsgConv msg =
    case msg of
        AddPlanItem.FpError errMsg -> FpError errMsg
        AddPlanItem.FpBOMadded bom -> BOMadded bom
        _ -> AddBOMcompMsg msg

type alias Model =
    { item : Maybe M.Item
    , bomsCompModel : Boms.Model
    , addBOMcomp : AddPlanItem.Model
    }

setModel : Item -> Model
setModel item =
    { item = Just item
    , bomsCompModel = Boms.initialModel 
    , addBOMcomp = AddPlanItem.initialModel item
    }

activate : Item -> Cmd Msg
activate item = Cmd.map bomsCompMsgConv <| Boms.fetchBomByItem item.id

update : GC.GlobalContext -> Msg -> Model -> (Model, Cmd Msg)
update gc msg model =
    case msg of
        Activate newItem ->
            let
                (subModel, cmd) = Boms.update gc (Boms.Activate newItem) model.bomsCompModel
            in
                ({model|item = Just newItem, bomsCompModel=subModel}, Cmd.map bomsCompMsgConv cmd)
        FpClose ->
            Debug.crash <| "This message should have been intercepted by my parent!"
        DoEdit item ->
            ({model|item = Nothing}, Cmd.none)
        FpError _ -> 
            Debug.crash <| "This message should have been intercepted by my parent!"
        BomsMsg subMsg ->
            let
                (subModel, subCmd) = Boms.update gc subMsg model.bomsCompModel
            in
                ({model| bomsCompModel = subModel}, Cmd.map bomsCompMsgConv subCmd)
        AddBOMcompMsg subMsg ->
            let
                (subModel, subCmd) = AddPlanItem.update gc subMsg model.addBOMcomp
            in
                ({model| addBOMcomp = subModel}, Cmd.map addBOMcompMsgConv subCmd)
        BOMadded bom ->
            let
                newBomsCompModel = Boms.addBom bom model.bomsCompModel 
            in
                ({model | bomsCompModel = newBomsCompModel} , Cmd.none) 


--##############################################################################################
compView : GC.GlobalContext -> Model -> Html Msg
compView gc model =
    case model.item of
        Just item ->
            div []
                [ div []
                    [ button [ onClick FpClose] [ text "Close" ]
                    , button [ onClick <| DoEdit item] [ text "Edit" ]
                    ]
                , h1 [] [ text <| item.description ]
                , Html.map bomsCompMsgConv <| Boms.viewBomPlanItems gc.itemTypes model.bomsCompModel
                , Html.map addBOMcompMsgConv <| AddPlanItem.newPlanItem gc model.addBOMcomp
                ]
        Nothing ->
            div [][]

