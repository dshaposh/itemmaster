module Items exposing (..)

import Json.Decode as Decode exposing (index, field)
import Http
import Html exposing (..)
import Html.Events exposing (onClick, onInput, onDoubleClick)
import Html.Attributes exposing (..)

import Task
import GlobalContext as GC exposing (..)
import Helper
import Models exposing (Item, itemDecoder)

import EditItem exposing (..)
import ViewItem exposing (..)

type alias ItemExt = (Item, String, String, Int, Int, String)

type ActiveComp
    = Me
    | EditItemComp EditItem.Model
    | ViewItemComp ViewItem.Model

type alias Filter =
    { group : String
    , itype : String
    , code1C : String
    , pn : String
    , description : String
    , pitype : String
    }

type alias Model =
    { items : List ItemExt
    , filter : Filter
    , editItemComp : EditItem.Model
    , activeComp : ActiveComp
    , piTypes : List ItemType
    }

emptyFilter =
    { group = ""
    , itype = "" 
    , code1C = "" 
    , pn = ""
    , description = ""
    , pitype = "" 
    }

initialModel : Model
initialModel =
    { items = []
    , filter = emptyFilter
    , editItemComp = EditItem.setModel Nothing
    , activeComp = Me
    , piTypes = []
    }


tupleDecoder : Decode.Decoder ItemExt
tupleDecoder =
    Decode.map6
        (,,,,,)
        (index 0 itemDecoder)
        (index 1 (index 0 Decode.string))
        (index 1 (index 1 Decode.string))
        (index 1 (index 2 Decode.int))
        (index 1 (index 3 Decode.int))
        (index 1 (index 4 Decode.string))

listTuplesDecoder : Decode.Decoder (List ItemExt)
listTuplesDecoder = Decode.list tupleDecoder

type Msg
    = FpDone (Maybe Item)
    | FpShowPlanItems GC.ItemType
    | FpViewItem Item
    | FpError String
    | FetchDone (Result Http.Error (List ItemExt))
    | FetchBomsIgroupSummaryDone (Result Http.Error (List Int))
    | DoFetch
    | FilterFieldSetTo String String
    | DeleteItem Item
    | DeleteDone (Result Http.Error Int)
    | DoAddItem
    | DoEditItem Item
    | CloseEditItem
    | DoViewItem Item
    | CloseViewItem
    | EditItemCompMsg EditItem.Msg
    | ViewItemCompMsg ViewItem.Msg


editItemCompMsgCnv : EditItem.Msg -> Msg
editItemCompMsgCnv msg =
    case msg of 
        EditItem.FpClose ->
            CloseEditItem
        EditItem.FpError errMsg ->
            FpError errMsg
        _ as internalMsg ->
            EditItemCompMsg internalMsg 

viewItemCompMsgConv : ViewItem.Msg -> Msg
viewItemCompMsgConv msg =
    case msg of 
        ViewItem.FpClose ->
            CloseViewItem
        ViewItem.FpError errMsg ->
            FpError errMsg
        _ as internalMsg ->
            ViewItemCompMsg internalMsg 

url : String
url = "/api/items"


queryPair : (String,String) -> String
queryPair (key,value) = queryEscape key ++ "=" ++ queryEscape value

queryEscape : String -> String
queryEscape string = String.join "+" (String.split "%20" (uriEncode string))

uriEncode : String -> String
uriEncode str = str
--uriEncode = Native.Http.uriEncode

createUrl : String -> List (String,String) -> String
createUrl baseUrl args =
    case args of
        [] -> baseUrl
        _ ->  baseUrl ++ "?" ++ String.join "&" (List.map queryPair args)

convertFilter : Filter -> List (String, String)
convertFilter filter =
    [ ("group", filter.group)
    , ("itype", filter.itype)
    , ("code1c", filter.code1C)
    , ("pn", filter.pn)
    , ("description", filter.description)
    , ("pitype", filter.pitype)
    ]

fetch : Filter -> Cmd Msg
fetch filter = Http.send FetchDone (Http.get (createUrl url (convertFilter filter)) listTuplesDecoder)

bomsSummaryByIgroupUrl : String -> String
bomsSummaryByIgroupUrl igroupId = "/api/boms/byitype/" ++ igroupId ++ "/summary"

fetchBomsSummary : String -> Cmd Msg
fetchBomsSummary igroupId = Http.send FetchBomsIgroupSummaryDone (Http.get (bomsSummaryByIgroupUrl igroupId) (Decode.list Decode.int))

delete : Item -> Cmd Msg
delete item = Http.send DeleteDone (Helper.httpDelete url item.id)

setFilter : Filter -> Model
setFilter filter =
    {initialModel| filter = filter}

update : GC.GlobalContext -> Msg -> Model -> (Model, Cmd Msg)
update gc msg model =
    case msg of
        FpDone subMsg ->
            Debug.crash <| "This message should have been intercepted by parent"
        FpShowPlanItems subMsg ->
            Debug.crash <| "This message should have been intercepted by parent"
        FpError _ ->
            Debug.crash <| "This message should have been intercepted by parent"
        FetchDone (Ok newItems) ->
            let cmd =
                if model.filter.itype /= "" 
                    then fetchBomsSummary model.filter.itype
                    else Cmd.none
            in
                ({model|items = newItems}, cmd)
        FetchDone (Err errMsg) ->
            (model, Task.succeed (toString errMsg) |> Task.perform FpError)
        FetchBomsIgroupSummaryDone (Ok typeIds) ->
            let types = List.filterMap (\id -> List.head <| List.filter (\itype -> itype.id == id) gc.itemTypes) typeIds
            in ({model|piTypes = types}, Cmd.none)
        FetchBomsIgroupSummaryDone (Err errMsg) ->
            (model, Task.succeed (toString errMsg) |> Task.perform FpError)
        DeleteItem item ->
            (model, delete item)
        DeleteDone (Ok id) ->
            let
                newList = List.filter
                    (\itemExt -> 
                        let (item, _, _, _, _, _) = itemExt
                        in item.id /= id)
                    model.items
            in
                ({model|items = newList}, Cmd.none)
        DeleteDone (Err errMsg) ->
            (model, Task.succeed (toString errMsg) |> Task.perform FpError)
        FpViewItem subMsg ->
            Debug.crash <| "My parent should have intercepted this message: " ++ toString subMsg
        DoAddItem ->
            ({model|activeComp = EditItemComp <| EditItem.setModel Nothing }, Task.perform (\a -> EditItemCompMsg <| EditItem.Activate a) (Task.succeed <| Nothing))
        DoEditItem item ->
            ({model|activeComp = EditItemComp <| EditItem.setModel <| Just item }, Task.perform (\a -> EditItemCompMsg <| EditItem.Activate a) (Task.succeed <| Just item))
        DoViewItem item ->
            ({model|activeComp = ViewItemComp <| ViewItem.setModel item }, Cmd.map viewItemCompMsgConv <| ViewItem.activate item)
        EditItemCompMsg subMsg ->
            let (cModel, cmd) = EditItem.update gc subMsg model.editItemComp
            in ({model| editItemComp = cModel}, Cmd.map editItemCompMsgCnv cmd)
        ViewItemCompMsg subMsg ->
            case model.activeComp of
                ViewItemComp subModel ->
                    let (newSubModel, cmd) = ViewItem.update gc subMsg subModel
                    in ({model| activeComp = ViewItemComp newSubModel}, Cmd.map viewItemCompMsgConv cmd)
                _ ->
                    (model, Cmd.none)
        CloseEditItem ->
            ({model|activeComp = Me}, Cmd.none)
        CloseViewItem ->
            ({model|activeComp = Me}, Cmd.none)
        FilterFieldSetTo fField fValue ->
            let filter1 = model.filter
                (filter2, cmd) = case fField of
                    "description" -> ({filter1|description = String.toLower fValue}, Cmd.none)
                    "pn" -> ({filter1|pn = String.toLower fValue}, Cmd.none)
                    "code1c" -> ({filter1|code1C = String.toLower fValue}, Cmd.none)
                    "itype" -> ({filter1|itype = String.toLower fValue}, Cmd.none)
                    "pitype" -> ({filter1|pitype = String.toLower fValue}, Task.succeed never |> Task.perform (\a -> DoFetch))
                    _ -> (filter1, Cmd.none)
            in ({model|filter = filter2}, cmd)
        DoFetch ->
            (model, fetch model.filter)

compView : GC.GlobalContext -> Model -> Html Msg
compView gc model =
    case model.activeComp of
        Me ->
            let
                mbSelectedType = List.filter (\itype -> toString itype.id == model.filter.itype) gc.itemTypes |> List.head
            in
                div [] (
                    [ h1 [] (
                        case mbSelectedType of
                            Nothing ->
                                [ text "Items" ]
                            Just selectedType ->
                                [ text <| selectedType.group ++ " - " ++ selectedType.name ++ " - " ++ selectedType.uom
                                ]
                    )
                    , button [onClick <| FpDone Nothing][text "Close"]
                    ]
                    ++ ( Maybe.withDefault [] <| Maybe.map 
                        (\stype -> [ button [onClick <| FpShowPlanItems stype][text "Planning items"]])
                        mbSelectedType
                    ) ++
                    [ button [onClick DoAddItem][text "Add"]
                    , filterView model
                    , listView model.items
                    ])
        EditItemComp model -> Html.map editItemCompMsgCnv <| EditItem.compView gc model
        ViewItemComp model -> Html.map viewItemCompMsgConv <| ViewItem.compView gc model

filterView : Model -> Html Msg
filterView model =
    div []
        [ input
            [ placeholder "Code1C"
            , type_ "number"
            , onInput (\str -> FilterFieldSetTo "code1c" str)
            , value <| model.filter.code1C
            ]
            []
        , input
            [ placeholder "PN"
            , type_ "number"
            , onInput (\str -> FilterFieldSetTo "pn" str)
            , value <| model.filter.pn
            ]
            []
        , input
            [ placeholder "Description"
            , onInput (\str -> FilterFieldSetTo "description" str)
            , value model.filter.description
            ]
            []
        , button [onClick DoFetch][text "Search"]
        , div [class "filter"]
            (if model.filter.itype =="" then []
            else
                (button
                    [ classList
                        [ ("selected", model.filter.pitype == "")
                        ]
                    , onClick <| FilterFieldSetTo "pitype" ""][text "All"] )
                ::
                (List.map
                    (\el ->
                        button
                            [ classList
                                [ ("selected", model.filter.pitype == toString el.id)
                                ]
                            , onClick <| FilterFieldSetTo "pitype" (toString el.id)][text el.name] )
                    model.piTypes)) 
        ]

listView : List ItemExt -> Html Msg
listView itemsExt =
    div []
        [ table []
            [ thead []
                [ tr []
                    [ th [] [ text "Group" ]
                    , th [] [ text "Type" ]
                    , th [] [ text "Code 1C" ]
                    , th [] [ text "PN" ]
                    , th [] [ text "Description" ]
                    , th [] [ text "UOM" ]
                    , th [] [ text "Price" ]
                    , th [] [ text "Planning Items Qty" ]
                    , th [] [ text "Planning Items Sum" ]
                    , th [] [ text "Planning item" ]
                    , th [] [ text "Action" ]
                    ]
                ]
            , tbody []
                (List.map itemView itemsExt)
            ]
        ]

itemView : ItemExt -> Html Msg
itemView itemExt =
    let
        (item, group, itype, bomCount, bomQty, pdescr) = itemExt
    in
        tr [onDoubleClick <| DoViewItem item ]
            [ td [] [ text group ]
            , td [] [ text itype ]
            , td [] [ text (toString item.code1C) ]
            , td [] [ text item.pn ]
            , td [] [ text item.description ]
            , td [] [ text item.uom ]
            , td [] [ text <| Helper.toStringM item.price ]
            , td [] [ text <| toString bomCount ]
            , td [] [ text <| toString bomQty ]
            , td [] [ text pdescr ]
            , td []
                [ button [onClick <| DeleteItem item][text "x"]
                , button [onClick <| DoEditItem item][text "e"]
                ]
            ]

selectView : Model -> Html Msg
selectView model =
    let
        items = model.items
    in
        div []
            [ button [onClick <| FpDone Nothing][text "Cancel"]
            , filterView model
            , table []
                [ thead []
                    [ tr []
                        [ th [] [ text "Group" ]
                        , th [] [ text "Type" ]
                        , th [] [ text "Code 1C" ]
                        , th [] [ text "PN" ]
                        , th [] [ text "Description" ]
                        , th [] [ text "UOM" ]
                        , th [] [ text "Price" ]
                        , th [] [ text "Planning Items Count" ]
                        , th [] [ text "Planning Items Qty" ]
                        ]
                    ]
                , tbody []
                    (List.map itemSelectView items)
                ]
            ]

itemSelectView : ItemExt -> Html Msg
itemSelectView itemExt =
    let
        (item, group, itype, bomCount, bomQty, _) = itemExt
    in
        tr [onDoubleClick <| FpDone (Just item)]
            [ td [] [ text group ]
            , td [] [ text itype ]
            , td [] [ text (toString item.code1C) ]
            , td [] [ text item.pn ]
            , td [] [ text item.description ]
            , td [] [ text item.uom ]
            , td [] [ text <| Helper.toStringM item.price ]
            , td [] [ text <| toString bomCount ]
            , td [] [ text <| toString bomQty ]
            ]
