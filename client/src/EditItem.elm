module EditItem exposing (..)

import Task
import Json.Encode as Encode
import Json.Decode as Decode exposing (field)
import Http
import Html exposing (..)
import Html.Events exposing (onClick, onInput)
import Html.Attributes exposing (class, value, type_, selected)

import Helper as Helper exposing (httpPut)
import Models exposing (Item)
import GlobalContext as GC exposing (..)

type Msg
    = FpClose
    | FpError String
    | Activate (Maybe Item)
    | DoClose
    | DoReset
    | DoSave
    | SaveDone (Result Http.Error Int)

    | Code1cSetTo String
    | PnSetTo String
    | DescriptionSetTo String
    | UomSetTo String
    | PriceSetTo String
    | PackageSizeSetTo String
    | TypeSetTo String


encodeItem : Model -> Encode.Value
encodeItem model =
    Encode.object
        [ ("code1C", Encode.int model.code1C)
        , ("pn", Encode.string model.pn)
        , ("description", Encode.string model.description)
        , ("uom", Encode.string model.uom)
        , ("itemType", Encode.int model.itemType)
        , ("price", Encode.float <| Result.withDefault 0 <|  String.toFloat model.price)
        , ("packageSize", Encode.float <| Result.withDefault 0 <| String.toFloat model.packageSize)
        ]

saveUrl : String
saveUrl = "/api/items"

save : Model ->Cmd Msg
save item =
    case item.id of
        0 -> Http.send SaveDone (Http.post saveUrl (Http.jsonBody <| encodeItem item) Decode.int) 
        id -> Http.send SaveDone (Helper.httpPut saveUrl id (Http.jsonBody <| encodeItem item) Decode.int) 


type alias Model =
    { item : Maybe Item
    , id : Int
    , code1C : Int
    , pn : String
    , description : String
    , uom : String
    , itemType : Int
    , price : String
    , packageSize : String
    }


setModel : Maybe Item -> Model
setModel mbItem =
    case mbItem of
        Nothing ->
            { item = Nothing
            , id = 0
            , code1C = 0
            , pn = ""
            , description = ""
            , uom = ""
            , itemType = 0
            , price = "0"
            , packageSize = "0"
            }
        Just item ->
            { item = Just item
            , id = item.id
            , code1C = item.code1C 
            , pn = item.pn 
            , description = item.description 
            , uom = item.uom 
            , itemType = item.itemType 
            , price = toString item.price 
            , packageSize = toString item.packageSize 
            }


update : GC.GlobalContext -> Msg -> Model -> (Model, Cmd Msg)
update gc msg model =
    let item = model.item
    in case msg of
        FpClose ->
            Debug.crash "This message should have been intercepted by parent"
        FpError _ -> 
            Debug.crash "This message should have been intercepted by parent"
        Activate mbItem ->
            (setModel mbItem, Cmd.none)
        DoClose ->
            (setModel Nothing, Task.perform (\a -> FpClose) (Task.succeed never))
        DoReset ->
            (setModel model.item, Cmd.none)
        DoSave ->
            (model, save model)
        SaveDone (Err errMsg) ->
            (model, Task.succeed (toString errMsg) |> Task.perform FpError )
        SaveDone (Ok id) ->
            (setModel Nothing, Task.perform (\a -> FpClose) (Task.succeed never))
        Code1cSetTo code1C ->
            ({model|code1C = Result.withDefault 0 <| String.toInt code1C},Cmd.none)
        PnSetTo pn ->
            ({model|pn = pn},Cmd.none)
        DescriptionSetTo description ->
            ({model|description = description},Cmd.none)
        UomSetTo uom ->
            ({model|uom = uom},Cmd.none)
        PriceSetTo price ->
            ({model|price = price},Cmd.none)
        TypeSetTo typeIdStr ->
            let typeId = String.toInt typeIdStr |> Result.withDefault 0 
            in ({model|itemType = typeId},Cmd.none)
        PackageSizeSetTo packageSize ->
            ({model|packageSize = packageSize},Cmd.none)

compView : GC.GlobalContext -> Model -> Html Msg
compView gc model =
    div [ class "form" ]
        [ h2 [] [ text "Add/Edit Item" ]
        , div [ class "formBody" ]
            [ div [ class "labelGroup" ]
                [ label [] [ text "Code1C" ]
                , div []
                    [ input [type_ "number", onInput Code1cSetTo, value <| toString model.code1C][]
                    ]
                ]
            , div [ class "labelGroup" ]
                [ label [] [ text "PN" ]
                , div []
                    [ input [onInput PnSetTo, value model.pn][]
                    ]
                ]
            , div [ class "labelGroup" ]
                [ label [] [ text "Description" ]
                , div []
                    [ input [onInput DescriptionSetTo, value model.description][]
                    ]
                ]
            , div [ class "labelGroup" ]
                [ label [] [ text "UOM" ]
                , div []
                    [ select [onInput UomSetTo, value model.uom]
                        (option [][] ::
                        (List.map (\uom -> option [selected (uom==model.uom) ] [text uom]) gc.uoms))
                    ]
                ]
            , div [ class "labelGroup" ]
                [ label [] [ text "Type" ]
                , div []
                    [ select [onInput TypeSetTo, value <| toString model.itemType]
                        (option [][] ::
                        (List.map
                            (\itype ->
                                option
                                    [value <| toString itype.id, selected (model.itemType == itype.id)]
                                    [text (itype.group ++ "-" ++ itype.name)])
                            gc.itemTypes))
                    ]
                ]
            , div [ class "labelGroup" ]
                [ label [] [ text "Price" ]
                , div []
                    [ input [type_ "number", onInput PriceSetTo, value model.price][]
                    ]
                ]
            , div [ class "labelGroup" ]
                [ label [] [ text "Package size" ]
                , div []
                    [ input [type_ "number", onInput PackageSizeSetTo, value model.packageSize][]
                    ]
                ]
            , div [ class "labelGroup" ]
                [ label [][]
                , div []
                    [ button [ onClick DoClose] [ text "Close" ]
                    , button [ onClick DoSave ] [ text "Save" ]
                    , button [ onClick DoReset] [ text "Reset" ]
                    ]
                ]
            ]
        ]
