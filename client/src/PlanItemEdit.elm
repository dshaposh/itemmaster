module PlanItemEdit exposing (..)

import Task
import Http
import Html exposing (..)
import Html.Events exposing (onClick, onInput)
import Html.Attributes exposing (type_, value, selected, disabled, class)
import Json.Decode as Decode exposing (field, string, int, float)
import Json.Encode as Encode

import Models as M exposing (..)
import GlobalContext as GC exposing (..)
import Helper as H exposing (..)

import PlanItems.Model as PlanItem exposing (..)

type Msg
    = FpDone (Maybe PlanItem.Model)
    | FpError String
    | Reset
    | Save
    | SaveDone (Result Http.Error Int)
    | Attr1SetTo String
    | Attr2SetTo String
    | Attr3SetTo String
    | Attr4SetTo String
    | Attr5SetTo String

 
type alias Model =
    { planItem : Maybe PlanItem.Attributes
    , attributes : PlanItem.Attributes
    , iType : ItemType
    , isValid : Bool
    }

initialModel : ItemType -> Maybe PlanItem.Attributes -> Model
initialModel itemType mbItem =
    case mbItem of
        Nothing ->
            { planItem = Nothing
            , attributes = PlanItem.emptyAttributes itemType.id
            , iType = itemType
            , isValid = False
            }
        Just item ->
            { planItem = Just item
            , attributes = item
            , iType = itemType
            , isValid = False
            }

resetModel : Model -> Model
resetModel model =
    { model
    | planItem = Nothing
    , isValid = False
    }

resetAttributes : Model -> Model
resetAttributes model =
    { model
    | attributes = PlanItem.emptyAttributes model.iType.id
    , isValid = False
    }

update : GC.GlobalContext -> Msg -> Model -> (Model, Cmd Msg)
update gc msg model =
    case msg of
        FpDone subMsg ->
            Debug.crash "This message should have been intercepted by the parent"
        FpError subMsg ->
            Debug.crash "This message should have been intercepted by the parent"
        Reset ->
            (resetAttributes model, Cmd.none)
        Save ->
            let
                id = Maybe.map (\item -> item.id) model.planItem
            in
                (model, PlanItem.save id model.attributes SaveDone)
        SaveDone (Err errMsg) ->
            (model, Task.succeed (toString errMsg) |> Task.perform FpError)
        SaveDone (Ok itemId) ->
            let
                newItem = PlanItem.Model itemId model.attributes 0 0 ""
            in
                (model, Task.succeed (Just newItem) |> Task.perform FpDone)
        Attr1SetTo value ->
            let
                attributes = model.attributes
                newAttributes = {attributes|attr1=value}
                isValid = validateModel newAttributes model.iType 
            in
                ({model|attributes = newAttributes, isValid = isValid}, Cmd.none)
        Attr2SetTo value ->
            let
                attributes = model.attributes
                newAttributes = {attributes|attr2=value}
                isValid = validateModel newAttributes model.iType 
            in
                ({model|attributes = newAttributes, isValid = isValid}, Cmd.none)
        Attr3SetTo value ->
            let
                attributes = model.attributes
                newAttributes = {attributes|attr3=value}
                isValid = validateModel newAttributes model.iType 
            in
                ({model|attributes = newAttributes, isValid = isValid}, Cmd.none)
        Attr4SetTo value ->
            let
                attributes = model.attributes
                newAttributes = {attributes|attr4=value}
                isValid = validateModel newAttributes model.iType 
            in
                ({model|attributes = newAttributes, isValid = isValid}, Cmd.none)
        Attr5SetTo value ->
            let
                attributes = model.attributes
                newAttributes = {attributes|attr5=value}
                isValid = validateModel newAttributes model.iType 
            in
                ({model|attributes = newAttributes, isValid = isValid}, Cmd.none)


itemEditView : GC.GlobalContext -> Model -> Html Msg
itemEditView gc model =
    let itemType = model.iType
        formName =
            case model.planItem of
                Nothing -> "Create a new planning item"
                Just item -> "Edit planning item"
    in 
        div [ class "form" ] 
            [ h2 [] [ text formName ] 
            , div [ class "formBody" ] (
                [ div [ class "labelGroup" ]
                    [ label [] [ text "Group" ]
                    , div []
                        [ select [disabled True ] (
                            (option [][text ""]) ::
                            (List.map
                                (\igroup ->
                                    option [value igroup, selected (igroup == model.iType.group)] [text igroup])
                                gc.itemGroups))
                        ]
                    ]
                , div [ class "labelGroup" ]
                    [ label [] [ text "Type" ]
                    , div []
                        [ select [ disabled True ] (
                            (option [][text ""]) ::
                            (List.map
                                (\itype ->
                                    let selectedTypeId = model.iType.id
                                    in option [value <| toString itype.id, selected (itype.id == selectedTypeId)] [text itype.name])
                                (List.filter (\itype -> itype.group == model.iType.group) gc.itemTypes)))
                        ]
                    ]
                , div [ class "labelGroup" ]
                    [ label [] [ text "UOM" ]
                    , div []
                        [ input [ value itemType.uom, disabled True ] []
                        ]
                    ]
                ]  
                ++ (attrLine Attr1SetTo model.attributes.attr1 itemType.attr1 itemType.attr1Values)
                ++ (attrLine Attr2SetTo model.attributes.attr2 itemType.attr2 itemType.attr2Values)
                ++ (attrLine Attr3SetTo model.attributes.attr3 itemType.attr3 itemType.attr3Values)
                ++ (attrLine Attr4SetTo model.attributes.attr4 itemType.attr4 itemType.attr4Values)
                ++ (attrLine Attr5SetTo model.attributes.attr5 itemType.attr5 itemType.attr5Values) 
                ++ [
                    div [ class "labelGroup" ]
                        [ label [] []
                        , div []
                            [ button [ onClick (FpDone Nothing) ] [ text "Close" ]
                            , button [ onClick Reset ] [ text "Reset" ]
                            , button [ onClick Save, disabled (not model.isValid)] [ text "Save" ]
                            ]
                        ]
                    ])
        ]

attrLine : (String -> Msg) -> String -> String -> String -> List (Html Msg)
attrLine msg val mAttrName mAttrValues =
    case mAttrName of
        "" -> []
        attrName ->
            let
                inputField =
                    case mAttrValues of
                        "" ->
                            text "Error"
                        "Number" ->
                            input [onInput msg, value val, type_ "number"][]
                        optList ->
                            select [onInput msg, value val] <|
                                (option [][] ::
                                (List.map
                                    (\optionValue -> option [selected (optionValue == val)][text optionValue])
                                    <| String.split "|" optList))
             in
                [ div [ class "labelGroup" ]
                    [ label [] [ text attrName ]
                    , div [] [ inputField ]
                    ]
                ]

