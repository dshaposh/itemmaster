module EditPlanItem exposing (..)

import Task
import Http
import Html exposing (..)
import Html.Events exposing (onClick, onInput)
import Html.Attributes exposing (type_, value, selected, disabled, class)
import Json.Decode as Decode exposing (field, string, int, float)
import Json.Encode as Encode

import Models as M exposing (..)
import GlobalContext as GC exposing (..)
import Helper as H exposing (..)

type Msg
    = FpDone (Maybe M.PlanItem)
    | FpError String
    | Reset
    | Save
    | SaveDone (Result Http.Error M.PlanItem)
    | FetchAttributesDone (Result Http.Error ItemAttributes)
    | GroupChangedTo String
    | TypeChangedTo String
    | Attr1SetTo String
    | Attr2SetTo String
    | Attr3SetTo String
    | Attr4SetTo String
    | Attr5SetTo String

type alias ItemAttributes =
    { price : Float
    , attr1 : String
    , attr2 : String
    , attr3 : String
    , attr4 : String
    , attr5 : String
    }

emptyAttributes : ItemAttributes
emptyAttributes =
    { price = 0
    , attr1 = ""
    , attr2 = ""
    , attr3 = ""
    , attr4 = ""
    , attr5 = ""
    }
 
type alias Model =
    { planItem : Maybe M.PlanItem
    , attributes : ItemAttributes
    , iGroup : String
    , iType : Maybe ItemType
    , isValid : Bool
    }

initialModel : GC.GlobalContext ->  Maybe M.PlanItem -> Model
initialModel gc maybePitem =
    case maybePitem of
        Nothing ->
            { planItem = Nothing
            , attributes = emptyAttributes
            , iGroup = ""
            , iType = Nothing
            , isValid = False
            }
        Just pItem ->
            let
                itype = List.filter (\el -> el.id == pItem.piTypeId) gc.itemTypes |> List.head
            in
                { planItem = Just pItem
                , attributes = emptyAttributes
                , iGroup = pItem.piGroup
                , iType = itype
                , isValid = False
                }

resetModel : Model -> String -> Maybe (ItemType) -> Model
resetModel model group itype =
    { model
    | attributes = emptyAttributes
    , iGroup = group
    , iType = itype
    , isValid = False
    }

validateModel : Model -> Bool
validateModel model =
    case model.iType of
        Nothing -> False
        Just itype ->
            (model.attributes.price > 0) &&
            (itype.attr1 == "") || (not <| String.isEmpty model.attributes.attr1) &&
            (itype.attr2 == "") || (not <| String.isEmpty model.attributes.attr2) &&
            (itype.attr3 == "") || (not <| String.isEmpty model.attributes.attr3) &&
            (itype.attr4 == "") || (not <| String.isEmpty model.attributes.attr4) &&
            (itype.attr5 == "") || (not <| String.isEmpty model.attributes.attr5)
    
saveUrl : String
saveUrl = "/api/planitems"

encodePlanItem : Model -> Encode.Value
encodePlanItem model =
    Encode.object
        [ ("itemType",
            case model.iType of
                Nothing -> Encode.string "null"
                Just itype -> Encode.int itype.id)
        , ("price", Encode.float model.attributes.price)
        , ("attr1", Encode.string model.attributes.attr1)
        , ("attr2", Encode.string model.attributes.attr2)
        , ("attr3", Encode.string model.attributes.attr3)
        , ("attr4", Encode.string model.attributes.attr4)
        , ("attr5", Encode.string model.attributes.attr5)
        ]

attributesDecoder : Decode.Decoder ItemAttributes
attributesDecoder =
    Decode.map6
        ItemAttributes
        (field "price" float)
        (field "attr1" string)
        (field "attr2" string)
        (field "attr3" string)
        (field "attr4" string)
        (field "attr5" string)

save : Model ->Cmd Msg
save model =
    case model.planItem of
        Nothing ->
            Http.send SaveDone (Http.post saveUrl (Http.jsonBody <| encodePlanItem model) M.planItemDecoder) 
        Just pitem ->
            Http.send SaveDone (H.httpPut saveUrl pitem.id (Http.jsonBody <| encodePlanItem model) M.planItemDecoder) 

fetchAttributes : M.PlanItem -> Cmd Msg
fetchAttributes item =
    Http.send FetchAttributesDone (Http.get (saveUrl ++ "/" ++ (toString item.id)) attributesDecoder)

update : GC.GlobalContext -> Msg -> Model -> (Model, Cmd Msg)
update gc msg model =
    case msg of
        FpDone subMsg ->
            Debug.crash "This message should have been intercepted by the parent"
        FpError subMsg ->
            Debug.crash "This message should have been intercepted by the parent"
        Reset ->
            (initialModel gc Nothing, Cmd.none)
        Save ->
            (model, save model)
        SaveDone (Err errMsg) ->
            (model, Task.succeed (toString errMsg) |> Task.perform FpError)
        SaveDone (Ok item) ->
            (model, Task.succeed (Just item) |> Task.perform FpDone)
        FetchAttributesDone (Err msg) ->
            Debug.crash <| "Error. Item attributes were not fetched." ++ (toString msg)
        FetchAttributesDone (Ok attributes) ->
            ({model|attributes = attributes}, Cmd.none)
        GroupChangedTo newGroup ->
            (resetModel model newGroup Nothing, Cmd.none)
        TypeChangedTo newTypeId ->
            let
                newType = List.head <| List.filter (\itype -> newTypeId == toString itype.id) gc.itemTypes
                newModel = resetModel model model.iGroup newType
                isValid = validateModel newModel
            in
                ({newModel|isValid = isValid}, Cmd.none)
        Attr1SetTo value ->
            let
                attributes = model.attributes
                newAttributes = {attributes|attr1=value}
                isValid = validateModel {model|attributes=newAttributes} 
            in
                ({model|attributes = newAttributes, isValid = isValid}, Cmd.none)
        Attr2SetTo value ->
            let
                attributes = model.attributes
                newAttributes = {attributes|attr2=value}
                isValid = validateModel {model|attributes=newAttributes} 
            in
                ({model|attributes = newAttributes, isValid = isValid}, Cmd.none)
        Attr3SetTo value ->
            let
                attributes = model.attributes
                newAttributes = {attributes|attr3=value}
                isValid = validateModel {model|attributes=newAttributes} 
            in
                ({model|attributes = newAttributes, isValid = isValid}, Cmd.none)
        Attr4SetTo value ->
            let
                attributes = model.attributes
                newAttributes = {attributes|attr4=value}
                isValid = validateModel {model|attributes=newAttributes} 
            in
                ({model|attributes = newAttributes, isValid = isValid}, Cmd.none)
        Attr5SetTo value ->
            let
                attributes = model.attributes
                newAttributes = {attributes|attr5=value}
                isValid = validateModel {model|attributes=newAttributes} 
            in
                ({model|attributes = newAttributes, isValid = isValid}, Cmd.none)


compView : GC.GlobalContext -> Model -> Html Msg
compView gc model =
    div []
        [ itemEditView gc model
        ]

itemEditView : GC.GlobalContext -> Model -> Html Msg
itemEditView gc model =
    let itemType = Maybe.withDefault GC.emptyType mItemType
        mItemType = model.iType
        formName =
            case model.planItem of
                Nothing -> "Crate new planning item"
                Just item -> item.piDescr
    in 
        div [ class "form" ] 
            [ h2 [] [ text formName ] 
            , div [ class "formBody" ] (
                [ div [ class "labelGroup" ]
                    [ label [] [ text "Group" ]
                    , div []
                        [ select [onInput GroupChangedTo ] (
                            (option [][text ""]) ::
                            (List.map
                                (\igroup ->
                                    option [value igroup, selected (igroup == model.iGroup)] [text igroup])
                                gc.itemGroups))
                        ]
                    ]
                , div [ class "labelGroup" ]
                    [ label [] [ text "Type" ]
                    , div []
                        [ select [onInput TypeChangedTo ] (
                            (option [][text ""]) ::
                            (List.map
                                (\itype ->
                                    let selectedTypeId =
                                        case model.iType of
                                            Nothing -> 0
                                            Just iType -> iType.id
                                    in option [value <| toString itype.id, selected (itype.id == selectedTypeId)] [text itype.name])
                                (List.filter (\itype -> itype.group == model.iGroup) gc.itemTypes)))
                        ]
                    ]
                , div [ class "labelGroup" ]
                    [ label [] [ text "UOM" ]
                    , div []
                        [ input [ value itemType.uom, disabled True ] []
                        ]
                    ]
                ]  
                ++ (attrLine Attr1SetTo model.attributes.attr1 itemType.attr1 itemType.attr1Values)
                ++ (attrLine Attr2SetTo model.attributes.attr2 itemType.attr2 itemType.attr2Values)
                ++ (attrLine Attr3SetTo model.attributes.attr3 itemType.attr3 itemType.attr3Values)
                ++ (attrLine Attr4SetTo model.attributes.attr4 itemType.attr4 itemType.attr4Values)
                ++ (attrLine Attr5SetTo model.attributes.attr5 itemType.attr5 itemType.attr5Values) 
                ++ [
                    div [ class "labelGroup" ]
                        [ label [] []
                        , div []
                            [ button [ onClick (FpDone Nothing) ] [ text "Close" ]
                            , button [ onClick Reset ] [ text "Reset" ]
                            , button [ onClick Save, disabled (not model.isValid)] [ text "Save" ]
                            ]
                        ]
                    ])
        ]

attrLine : (String -> Msg) -> String -> String -> String -> List (Html Msg)
attrLine msg val mAttrName mAttrValues =
    case mAttrName of
        "" -> []
        attrName ->
            let
                inputField =
                    case mAttrValues of
                        "" ->
                            text "Error"
                        "Number" ->
                            input [onInput msg, value val, type_ "number"][]
                        optList ->
                            select [onInput msg, value val] <|
                                (option [][] ::
                                (List.map
                                    (\optionValue -> option [selected (optionValue == val)][text optionValue])
                                    <| String.split "|" optList))
             in
                [ div [ class "labelGroup" ]
                    [ label [] [ text attrName ]
                    , div [] [ inputField ]
                    ]
                ]


itemView : GC.GlobalContext -> Model -> Html Msg
itemView gc model =
    let a = model.attributes
    in
        case model.iType of
            Nothing -> div [][text "Error, item type has not been found"]
            Just itype ->
                table []
                    [ thead []
                        [ tr []
                            [ th [] [text "Description"]
                            , th [] [text "Value"]
                            ]
                       ]
                    , tbody []
                        [ tr []
                            [ td [] [ text "UOM" ]
                            , td [] [ text itype.uom ]
                            ]
                        , tr []
                            [ td [] [ text "Price" ]
                            , td [] [ toString a.price |> text ]
                            ]
                        , tr []
                            [ td [] [ text itype.attr1 ]
                            , td [] [ text a.attr1 ]
                            ]
                        , tr []
                            [ td [] [ text itype.attr2 ]
                            , td [] [ text a.attr2 ]
                            ]
                        , tr []
                            [ td [] [ text itype.attr3 ]
                            , td [] [ text a.attr3 ]
                            ]
                        , tr []
                            [ td [] [ text itype.attr4 ]
                            , td [] [ text a.attr4 ]
                            ]
                        , tr []
                            [ td [] [ text itype.attr5 ]
                            , td [] [ text a.attr5 ]
                            ]
                        ]
                    ]
