﻿CREATE OR REPLACE FUNCTION itemdescr (r plan_item, tmpl text) RETURNS text AS $$
DECLARE

	description text :='';

BEGIN

	IF tmpl is null THEN
		RETURN null;
	END IF;

	IF r.attr1 is not null THEN
		description =replace(tmpl, '|1|', r.attr1);
	END IF;
	IF r.attr2 is not null THEN
		description =replace(description, '|2|', r.attr2);
	END IF;
	IF r.attr3 is not null THEN
		description =replace(description, '|3|', r.attr3);
	END IF;
	IF r.attr4 is not null THEN
		description =replace(description, '|4|', r.attr4);
	END IF;
	IF r.attr5 is not null THEN
		description =replace(description, '|5|', r.attr5);
	END IF;

	RETURN description;
END;
$$ LANGUAGE plpgsql;
